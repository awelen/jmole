# JMole - a monitoring framework for Java MBeans

Copyright Anders Wel�n, anders@welen.net
(See also "copyright.txt")

For more info and documentation: https://bitbucket.org/awelen/jmole/wiki/Home

## Thirdparty components ##
JMole is internally using several different OpenSource libraries with different licenses.
