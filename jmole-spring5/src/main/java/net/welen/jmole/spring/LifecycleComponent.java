package net.welen.jmole.spring;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import net.welen.jmole.Lifecycle;

/**
 * A Spring conponent that is used to handle the lifecycle of JMole.
 */
@Component
public class LifecycleComponent {

	private final static Logger LOG = Logger.getLogger(LifecycleComponent.class.getName());
	
	@PostConstruct
	private void setupNotStatic() {
		LOG.log(Level.FINE, "Starting JMole");
		Lifecycle.setup();
	}

	@PreDestroy
	private void cleanupNotStatic() {
		LOG.log(Level.FINE, "Stopping JMole");
		Lifecycle.cleanup();
	}
}
