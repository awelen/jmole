package net.welen.jmole.microprofile;

/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;

import net.welen.jmole.JMole;
import net.welen.jmole.cdi.LifecycleBean;

@Health
@ApplicationScoped
public class JMoleHealth implements HealthCheck {

	private static final String INCLUDE_WARNINGS = "jmole.protocol.microprofile.health.includeWarnings";
	private static final String DOWN_IF_WARNINGS = "jmole.protocol.microprofile.health.downIfWarnings";
	private static final String EXCLUDE_ERRORS = "jmole.protocol.microprofile.health.excludeErrors";
	private static final String INCLUDE_MEASUREMENTS = "jmole.protocol.microprofile.health.includeMeasurements";

	@Inject
	LifecycleBean lifecycleBean;

	@Override
	public HealthCheckResponse call() {
		JMole jmole = lifecycleBean.getJMoleInstance();
		
		HealthCheckResponseBuilder answer = HealthCheckResponse.named("JMole");

		answer.up();

		try {
			Map<String, Map<String, String>> criticals = jmole.criticalMessages();
			Map<String, Map<String, String>> warnings = jmole.warningMessages();

			if (!criticals.isEmpty()) {
				answer.down();
				if (!Boolean.getBoolean(EXCLUDE_ERRORS)) {
					addValues("CRITICAL ", answer, criticals);
				}
			}
	
			if (!warnings.isEmpty()) {
				if (Boolean.getBoolean(DOWN_IF_WARNINGS)) {
					answer.down();
				}
				if (Boolean.getBoolean(INCLUDE_WARNINGS)) {
					addValues("WARNING ", answer, warnings);
				}
			}
	
			if (Boolean.getBoolean(INCLUDE_MEASUREMENTS)) {
				addMeasurement(answer, jmole.collectMeasurements());
			}
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
			
		return answer.build();
	}

	private void addValues(String prefix, HealthCheckResponseBuilder answer, Map<String, Map<String, String>> values) {
		for (Entry<String, Map<String, String>> categoryEntry : values.entrySet()) {
			String category = categoryEntry.getKey();
			for (Entry<String, String> entry : categoryEntry.getValue().entrySet()) {
				answer.withData(prefix + category + " " + entry.getKey(), entry.getValue());
			}
		}
	}

	private void addMeasurement(HealthCheckResponseBuilder answer,
			Map<String, List<Map<String, Map<String, Object>>>> collectMeasurements) {

		for (Entry<String, List<Map<String, Map<String, Object>>>> categoryEntry : collectMeasurements.entrySet()) {
			String category = categoryEntry.getKey();
			Iterator<Map<String, Map<String, Object>>> iter = categoryEntry.getValue().iterator();
			while (iter.hasNext()) {				
				for (Entry<String, Map<String, Object>> nameEntry : iter.next().entrySet()) {
					String name = nameEntry.getKey();
					for (Entry<String, Object> attributeEntry : nameEntry.getValue().entrySet()) {
						String attribute = attributeEntry.getKey();
						Object value = attributeEntry.getValue();
						if (value ==  null) {														
							answer.withData(category + " " + name + " " + attribute, "null");
						} else {
							answer.withData(category + " " + name + " " + attribute, value.toString());
						}
					}
				}
			}
		}
	}

}
