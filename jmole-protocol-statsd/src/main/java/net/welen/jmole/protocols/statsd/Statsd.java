package net.welen.jmole.protocols.statsd;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.protocols.AbstractIntervalProtocol;

public class Statsd extends AbstractIntervalProtocol implements StatsdMBean {

	private final static Logger LOG = Logger.getLogger(Statsd.class.getName());

	private static final char SEP = '.';
	
	private static String PROPERTY_STATSD_PREFIX = "jmole.protocol.statsd.prefix";
	private static String PROPERTY_STATSD_HOST = "jmole.protocol.statsd.host";
	private static String PROPERTY_STATSD_PORT = "jmole.protocol.statsd.port";
	private static String PROPERTY_STATSD_INTERVAL = "jmole.protocol.statsd.interval";

	private StatsDClient statsd = null;
	private String prefix;
	private String host;
	private Integer port;
	
	@Override
	public void startProtocol(JMole jmole) throws Exception {
		prefix = System.getProperty(PROPERTY_STATSD_PREFIX);
		if (prefix == null) {
			prefix = "JMole";
		}

		host = System.getProperty(PROPERTY_STATSD_HOST);
		if (host == null) {
			host = "localhost";
		}

		port = Integer.getInteger(PROPERTY_STATSD_PORT);
		if (port == null) {
			port = 8125;
		}

		Long interval = Long.getLong(PROPERTY_STATSD_INTERVAL);
		if (interval != null) {
			setInterval(interval);
		}

		statsd = new NonBlockingStatsDClient(prefix, host, port);
		super.startProtocol(jmole);
		LOG.log(Level.INFO, "JMole StatsD protocol started: " + prefix + " " + host + " " + port);
	}
	

	@Override
	public void stopProtocol() throws Exception {
		LOG.log(Level.INFO, "Stopping JMole StatsD protocol");
		super.stopProtocol();
		LOG.log(Level.INFO, "JMole StatsD protocol stopped");
	}
	
	@Override
	public String getPrefix() {
		return prefix;
	}

	@Override
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String getHost() {
		return host;
	}

	@Override
	public void setHost(String host) {
		this.host = host;
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public void setPort(int port) {
		this.port = port;
	}

	@Override
	protected void handleMeasurement(String category, String name, String attribute, Object value,
			PresentationInformation presentationInformation) throws Exception {

		String key = String.format("%s%c%s%c%s", 
				fixKeyName(category),
				SEP, fixKeyName(name),
				SEP, fixKeyName(attribute));

		Double dValue = Double.parseDouble(value.toString());
		LOG.log(Level.FINE, key + " = " + value);														
		statsd.recordGaugeValue(key, dValue);
	}	
	
	private String fixKeyName(String key) {
		return key.replaceAll("[^a-zA-Z0-9]", "_");
	}	

        @Override
        public void handleWarning(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		statsd.recordSetEvent(presentationInformation.getCategory() + SEP + name, message);
        }

        @Override
        public void handleCritical(String name, String attribute,PresentationInformation presentationInformation, String message) throws Exception {
		statsd.recordSetEvent(presentationInformation.getCategory() + SEP + name, message);
        }

}
