package net.welen.jmole.protocols.syslog;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.CharArrayWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cloudbees.syslog.Facility;
import com.cloudbees.syslog.MessageFormat;
import com.cloudbees.syslog.Severity;
import com.cloudbees.syslog.SyslogMessage;
import com.cloudbees.syslog.sender.AbstractSyslogMessageSender;
import com.cloudbees.syslog.sender.SyslogMessageSender;
import com.cloudbees.syslog.sender.TcpSyslogMessageSender;
import com.cloudbees.syslog.sender.UdpSyslogMessageSender;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.protocols.AbstractIntervalProtocol;
import net.welen.jmole.protocols.Utils;

public class Syslog extends AbstractIntervalProtocol implements SyslogMBean {

	private final static Logger LOG = Logger.getLogger(Syslog.class.getName());

	private static final char SEP = '/';
	
	private static String PROPERTY_SYSLOG_USE_TCP = "jmole.protocol.syslog.useTCP";
	private static String PROPERTY_SYSLOG_MESSAGE_HOSTNAME = "jmole.protocol.syslog.messageHostName";
	private static String PROPERTY_SYSLOG_APPLICATION_NAME = "jmole.protocol.syslog.applicationName";
	private static String PROPERTY_SYSLOG_FORMAT = "jmole.protocol.syslog.format";
	private static String PROPERTY_SYSLOG_LOG_FORMAT = "jmole.protocol.syslog.logFormat";
	private static String PROPERTY_SYSLOG_USE_SSL = "jmole.protocol.syslog.useSSL";
	private static String PROPERTY_SYSLOG_HOST = "jmole.protocol.syslog.host";
	private static String PROPERTY_SYSLOG_PORT = "jmole.protocol.syslog.port";
	private static String PROPERTY_SYSLOG_INTERVAL = "jmole.protocol.syslog.interval";
	
	private Boolean useTCP;
	private String host;
	private Integer port;
	private String messageHostName;	
	private String applicationName;	
	private MessageFormat format = SyslogMessageSender.DEFAULT_SYSLOG_MESSAGE_FORMAT;
	private String logFormat;
	private Boolean useSSL;

	private AbstractSyslogMessageSender messageSender;
		
	@Override
	public void startProtocol(JMole jmole) throws Exception {
		useTCP = Boolean.getBoolean(PROPERTY_SYSLOG_USE_TCP);
		if (useTCP == null) {
			useTCP = false;
		}
		
		host = System.getProperty(PROPERTY_SYSLOG_HOST);
		if (host == null) {
			host = "localhost";
		}
		
		port = Integer.getInteger(PROPERTY_SYSLOG_PORT);
		if (port == null) {
			port = 514;
		}
		
		if (useTCP) {			
			TcpSyslogMessageSender tcpMessageSender = new TcpSyslogMessageSender();
			
			tcpMessageSender.setSocketConnectTimeoutInMillis(10000);
			tcpMessageSender.setSyslogServerHostname(host);
			tcpMessageSender.setSyslogServerPort(port);

			useSSL = Boolean.getBoolean(PROPERTY_SYSLOG_USE_SSL);
			if (useSSL == null) {
				useSSL = false;
			}
			tcpMessageSender.setSsl(useSSL);
			
			messageSender = tcpMessageSender;
		} else {			
			UdpSyslogMessageSender udpMessageSender = new UdpSyslogMessageSender();
			
			udpMessageSender.setSyslogServerHostname(host);
			udpMessageSender.setSyslogServerPort(port);
			
			messageSender = udpMessageSender;
		}
	
		messageSender.setDefaultFacility(Facility.AUDIT);
		messageSender.setDefaultSeverity(Severity.INFORMATIONAL);
		
		messageHostName = System.getProperty(PROPERTY_SYSLOG_MESSAGE_HOSTNAME);
		if (messageHostName != null) {
			messageSender.setDefaultMessageHostname(messageHostName);
		}

		applicationName = System.getProperty(PROPERTY_SYSLOG_APPLICATION_NAME);
		if (applicationName == null) {
			applicationName = "JMole";
		}
		messageSender.setDefaultAppName(applicationName);
		
		String tmp = System.getProperty(PROPERTY_SYSLOG_FORMAT);		
		if (tmp != null) {
			format = MessageFormat.valueOf(System.getProperty(PROPERTY_SYSLOG_FORMAT));
		}
		messageSender.setMessageFormat(format);

		logFormat = System.getProperty(PROPERTY_SYSLOG_LOG_FORMAT);		
		if (logFormat == null) {
			logFormat = "[%k] %a [%A] = %v %U";
		}
		
		Long interval = Long.getLong(PROPERTY_SYSLOG_INTERVAL);
		if (interval != null) {
			setInterval(interval);
		}

		super.startProtocol(jmole);
		LOG.log(Level.INFO, "JMole Syslog protocol started: " + host + ":" + port + " interval=" + getInterval());
	}
	

	@Override
	public void stopProtocol() throws Exception {
		LOG.log(Level.INFO, "Stopping JMole Syslog protocol");
		super.stopProtocol();
		LOG.log(Level.INFO, "JMole Syslogd protocol stopped");
	}

	@Override
	public boolean getUseTCP() {
		return useTCP;
	}

	@Override
	public String getMessageHostName() {
		return messageHostName;
	}

	@Override
	public String getApplicationName() {
		return applicationName;
	}

	@Override
	public String getFormat() {
		return format.name();
	}

	@Override
	public boolean getUseSSL() {
		return useSSL;
	}
	
	@Override
	public String getHost() {
		return host;
	}

	@Override
	public int getPort() {
		return port;
	}
	@Override
	public String getLogFormat() {
		return logFormat;
	}

	@Override
	public void setLogFormat(String format) {
		logFormat = format;
	}
	
	@Override
	protected void handleMeasurement(String category, String name, String attribute, Object value,
			PresentationInformation presentationInformation) throws Exception {
		SyslogMessage msg = new SyslogMessage();
                msg.setAppName(applicationName);
		msg.setSeverity(Severity.INFORMATIONAL);
		msg.setFacility(Facility.USER);
		CharArrayWriter text = new CharArrayWriter();
                text.write(Utils.formatLogString(logFormat, SEP, category, name, attribute, value, presentationInformation));
                msg.setMsg(text);
		messageSender.sendMessage(msg);
	}

	@Override
        public void handleWarning(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		SyslogMessage msg = new SyslogMessage();
		msg.setAppName(applicationName);
		msg.setSeverity(Severity.WARNING);
		msg.setFacility(Facility.ALERT);
		CharArrayWriter text = new CharArrayWriter();
		text.write("[" + presentationInformation.getCategory() + SEP + name + "] " + message);
		msg.setMsg(text);
		messageSender.sendMessage(msg);
        }

        @Override
        public void handleCritical(String name, String attribute,PresentationInformation presentationInformation, String message) throws Exception {
		                SyslogMessage msg = new SyslogMessage();
                msg.setAppName(applicationName);
                msg.setSeverity(Severity.ERROR);
                msg.setFacility(Facility.ALERT);
                CharArrayWriter text = new CharArrayWriter();
                text.write("[" + presentationInformation.getCategory() + SEP + name + "] " + message);
                msg.setMsg(text);
                messageSender.sendMessage(msg);
        }

}
