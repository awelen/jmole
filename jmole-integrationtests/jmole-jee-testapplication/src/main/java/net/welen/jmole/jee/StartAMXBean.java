package net.welen.jmole.jee;

/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Singleton
@Startup
public class StartAMXBean {

	private final static Logger LOG = Logger.getLogger(StartAMXBean.class.getName());


	@PostConstruct
	private void start() {
		try {
			MBeanServer server = ManagementFactory.getPlatformMBeanServer();
			ObjectName objectName = new ObjectName("amx-support:type=boot-amx");
			if (server.isRegistered(objectName)) {
				LOG.info("Glassfish detected. Enabling JMX monitoring");
				server.invoke(objectName, "bootAMX", null, null);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
