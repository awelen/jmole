package net.welen.jmole.jee;

/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Logger;

import javax.ejb.Stateful;

import javax.annotation.Resource;
import javax.inject.Inject;

import javax.jms.Queue;
import javax.jms.Topic;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;

@Stateful
public class SFSBBean {

	private final static Logger LOG = Logger.getLogger(SFSBBean.class.getName());

	@Inject
	private JMSContext context;

	@Resource(mappedName="jms/testQueue")
	private Queue testQueue;

	@Resource(mappedName="jms/testTopic")
	private Topic testTopic;

	private long count = 0;
	
	public void send() {
		LOG.info("Sending message");

		String message = "Test Message: " + count++;

		JMSProducer producer = context.createProducer();
		producer.send(testQueue, message);
		producer.send(testTopic, message);
	}

}
