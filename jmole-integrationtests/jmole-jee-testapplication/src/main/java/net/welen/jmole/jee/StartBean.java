package net.welen.jmole.jee;

/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Logger;
import java.util.Properties;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.EJB;

import javax.batch.api.AbstractBatchlet;
import javax.ejb.Schedule;
import javax.batch.runtime.BatchRuntime;

import javax.inject.Named;

import static javax.batch.runtime.BatchStatus.COMPLETED;

@Named
@Singleton
@Startup
public class StartBean extends AbstractBatchlet {

	private final static Logger LOG = Logger.getLogger(StartBean.class.getName());

	@EJB
	SFSBBean bean;
	
	@Override
	public String process() {
		LOG.info("Creating Message");
		bean.send();
 		return COMPLETED.toString();
	}

	@Schedule(hour = "*", minute = "*", second = "*/10", persistent = false)
	public void runBatch() {
		BatchRuntime.getJobOperator().start("testJob", new Properties());
	}

}
