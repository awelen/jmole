package net.welen.jmole.protocol;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Map;
import javax.management.ObjectName;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import net.welen.jmole.JMole;
import net.welen.jmole.Lifecycle;

public class LoggerProtocolTest extends AbstractProtocolTest {

	private class TestLogHandler extends Handler {
                int numInfo = 0;
                int numWarn = 0;
                int numSevere = 0;

                @Override
                public void publish(LogRecord record) {
                        if (record.getLevel() == Level.INFO) {
                                numInfo++;
                        }
                        if (record.getLevel() == Level.WARNING) {
                                numWarn++;
                        }
                        if (record.getLevel() == Level.SEVERE) {
                                numSevere++;
                        }
                }

                @Override
                public void flush() {
                }

                @Override
                public void close() throws SecurityException {
                }
        }

        @Test
        public void testProtocolLogger() throws Exception {
                TestLogHandler testHandler = new TestLogHandler();
                try {
                        System.setProperty("jmole.protocol.logger.enabled", "true");
                        System.setProperty("jmole.protocol.logger.interval", "1000");
                        Lifecycle.setup();
                        Logger.getLogger("JMole").addHandler(testHandler);
                        Thread.sleep(1500);

                        assertEquals(1, testHandler.numInfo);
                        assertEquals(1, testHandler.numWarn);
                        assertEquals(1, testHandler.numSevere);
                } finally {
                        Lifecycle.cleanup();
                        System.clearProperty("jmole.protocol.logger.enabled");
                        System.clearProperty("jmole.protocol.logger.interval");
                        Logger.getLogger("JMole").removeHandler(testHandler);
                }
        }

}
