package net.welen.jmole.protocol;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Map;
import javax.management.ObjectName;

import net.welen.jmole.JMole;
import net.welen.jmole.Lifecycle;

public class JMXProtocolTest extends AbstractProtocolTest {

	@Test
	public void testProtocolJMX() throws Exception {
		try {
			Lifecycle.setup();
			Thread.sleep(1500);

			ObjectName jmoleName = new ObjectName(JMole.OBJECT_NAME);

			assertEquals(1, ((Map) server.invoke(jmoleName, "collectMeasurements", null, null)).size());
			assertEquals(1, ((Map) server.invoke(jmoleName, "warningMessages", null, null)).size());
			assertEquals(1, ((Map) server.invoke(jmoleName, "criticalMessages", null, null)).size());
		} finally {
			Lifecycle.cleanup();
		}
	}

}
