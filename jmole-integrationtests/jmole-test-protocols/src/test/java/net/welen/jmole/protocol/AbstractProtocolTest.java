package net.welen.jmole.protocol;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.Notification;

import org.junit.After;
import org.junit.Before;

import net.welen.jmole.Lifecycle;
import net.welen.jmole.Utils;

import net.welen.jmole.protocol.service.TestService;

public abstract class AbstractProtocolTest {
	
	MBeanServer server = Utils.getMBeanServer();
	ObjectName objectName; 

	@Before
	public void setup() throws MalformedObjectNameException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {		
		objectName = new ObjectName("jmole.testapp:service=testService");
                TestService testMBean = new TestService();
		server.registerMBean(testMBean, objectName);

		System.setProperty("jmole.config.filename.testservice", "src/test/resources/JMole_Test.xml");
	}

	@After
	public void cleanup() throws MBeanRegistrationException, InstanceNotFoundException {
		server.unregisterMBean(objectName);
	}

}
