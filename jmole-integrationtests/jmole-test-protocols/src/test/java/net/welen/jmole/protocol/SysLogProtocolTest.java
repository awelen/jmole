package net.welen.jmole.protocol;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.ClassRule;

import net.welen.jmole.Lifecycle;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Container;

public class SysLogProtocolTest extends AbstractProtocolTest {
	
        @ClassRule
        public static GenericContainer<?> container = new GenericContainer("balabit/syslog-ng:3.36.1").withExposedPorts(601);

        @Test
        public void testProtocolSyslog() throws Exception {
                try {
                        System.setProperty("jmole.protocol.syslog.enabled", "true");
                        System.setProperty("jmole.protocol.syslog.interval", "1000");
                        System.setProperty("jmole.protocol.syslog.useTCP", "true");
                        System.setProperty("jmole.protocol.syslog.format", "RFC_5425");
                        System.setProperty("jmole.protocol.syslog.host", container.getHost());
                        System.setProperty("jmole.protocol.syslog.port", container.getMappedPort(601).toString());
                        Lifecycle.setup();
                        Thread.sleep(5000); // 1500 Should be ok if we can get the server to flush to disk

			Container.ExecResult result = container.execInContainer("/bin/cat", "/var/log/messages");
			assertEquals(0, result.getExitCode());
			String log = result.getStdout();
                        assertTrue(log.contains("JMole: [Test/TestApp/TestValue] TestValue [] = 100 Count"));
                        assertTrue(log.contains("JMole: [Test/TestApp] TestValue: 100.0 > 39"));
                        assertTrue(log.contains("JMole: [Test/TestApp] TestValue: 100.0 > 80"));
                } finally {
                        Lifecycle.cleanup();
                        System.clearProperty("jmole.protocol.syslog.enabled");
                        System.clearProperty("jmole.protocol.syslog.useTCP");
                        System.clearProperty("jmole.protocol.syslog.format");
                        System.clearProperty("jmole.protocol.syslog.host");
                        System.clearProperty("jmole.protocol.syslog.port");
                        System.clearProperty("jmole.protocol.syslog.interval");
                }
        }

}
