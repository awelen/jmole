package net.welen.jmole.protocol;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.ClassRule;

import net.welen.jmole.Lifecycle;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.BindMode;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.json.JSONException;

public class LogstashProtocolTest extends AbstractProtocolTest {
	
        @ClassRule
        public static GenericContainer<?> container = new GenericContainer("logstash:7.16.2")
								.withFileSystemBind("src/test/resources/logstash.conf", "/usr/share/logstash/pipeline/logstash.conf", BindMode.READ_ONLY)
								.withExposedPorts(5000);

        @Test
        public void testProtocolLogstash() throws Exception {
                try {
                        System.setProperty("jmole.protocol.logstash.enabled", "true");
                        System.setProperty("jmole.protocol.logstash.interval", "1000");
                        System.setProperty("jmole.protocol.logstash.host", container.getHost());
                        System.setProperty("jmole.protocol.logstash.port", container.getMappedPort(5000).toString());
                        Lifecycle.setup();
                        Thread.sleep(1500);

			Container.ExecResult result = container.execInContainer("/bin/cat", "/tmp/jmole.log");
                        assertEquals(0, result.getExitCode());
                        String log = result.getStdout().replace("}", "}\n");

			String measurement = null;
			String warning = null;
			String critical = null;

			String lines[] = log.split("\\n");

			for (String line : lines) {
				if (line.contains("type\":\"metric\"")) {
                                        measurement = line;
                                }
				if (line.contains("type\":\"warning\"")) {
                                        warning = line;
                                }

				if (line.contains("type\":\"critical\"")) {
					critical = line;
				}
			}

			assertNotNull(measurement);
			JSONAssert.assertEquals("{\"value\":100,\"type\":\"metric\",\"name\":\"TestApp\",\"category\":\"Test\",\"attribute\":\"TestValue\",\"unit\":\"Count\"}"
					, measurement, JSONCompareMode.LENIENT);

			assertNotNull(critical);
			JSONAssert.assertEquals("{\"type\":\"critical\",\"message\":\"TestValue: 100.0 > 80\",\"name\":\"TestApp\",\"category\":\"Test\",\"unit\":\"Count\",\"attribute\":\"TestValue\"}"
					, critical, JSONCompareMode.LENIENT);

			assertNotNull(warning);
			JSONAssert.assertEquals("{\"type\":\"warning\",\"message\":\"TestValue: 100.0 > 39\",\"name\":\"TestApp\",\"category\":\"Test\",\"unit\":\"Count\",\"attribute\":\"TestValue\"}"
					, warning, JSONCompareMode.LENIENT);
					
                } finally {
                        Lifecycle.cleanup();
                        System.clearProperty("jmole.protocol.logstash.enabled");
                        System.clearProperty("jmole.protocol.logstash.host");
                        System.clearProperty("jmole.protocol.logstash.port");
                        System.clearProperty("jmole.protocol.logstash.interval");
                }
        }

}
