package net.welen.jmole.protocol;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.ClassRule;

import net.welen.jmole.Lifecycle;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.BindMode;
import org.testcontainers.Testcontainers;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.json.JSONException;

public class MuninProtocolTest extends AbstractProtocolTest {

        private final static Logger LOG = Logger.getLogger(MuninProtocolTest.class.getName());

	
        @ClassRule
        public static GenericContainer<?> container = new GenericContainer("occitech/munin")
								.withEnv("DISABLELOCALNODE", "yes")
								.withEnv("NODES", "host.testcontainers.internal")
								.withAccessToHost(true) 
								.withExposedPorts(80);
	static {
		Testcontainers.exposeHostPorts(4949);
	}

        @Test
        public void testProtocolMunin() throws Exception {
                try {
                        System.setProperty("jmole.protocol.munin.enabled", "true");
                        System.setProperty("jmole.protocol.munin.nodeName", "host.testcontainers.internal");

			long startTime = System.currentTimeMillis();
                        Lifecycle.setup();

			LOG.log(Level.INFO, "This testcase will sleep for 6 min as it takes Munin 5 min to collect values");
			Thread.sleep(60*1000*6); 

			// Check value
			Container.ExecResult result = container.execInContainer("/usr/bin/rrdtool", "lastupdate", "/var/lib/munin/testcontainers.internal/host.testcontainers.internal-TestApp-TestValue-g.rrd");
                        assertEquals(0, result.getExitCode());
			String lines[] = result.getStdout().split("\n");
			String values[] = lines[lines.length - 1].split(":");
			assertEquals(Integer.parseInt(values[1].trim()), 100);

			// Check that we have a critical value
			result = container.execInContainer("/bin/cat", "/var/lib/munin/limits");
                        assertEquals(0, result.getExitCode());
			assertTrue(result.getStdout().contains("testcontainers.internal;host.testcontainers.internal;TestApp;TestValue;critical Value is 100.00. Critical range (:80) exceeded"));

                } finally {
                        Lifecycle.cleanup();
                        System.clearProperty("jmole.protocol.munin.enabled");
                        System.clearProperty("jmole.protocol.munin.nodeName");
                }
        }

}
