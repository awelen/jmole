package net.welen.jmole.protocol;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.ClassRule;

import net.welen.jmole.Lifecycle;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.BindMode;
import org.testcontainers.Testcontainers;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.json.JSONException;

public class NRPEProtocolTest extends AbstractProtocolTest {

        private final static Logger LOG = Logger.getLogger(NRPEProtocolTest.class.getName());

	
        @ClassRule
        public static GenericContainer<?> container = new GenericContainer("jasonrivers/nagios")
								.withFileSystemBind("src/test/resources/nagios.cfg", "/opt/nagios/etc/objects/jmole.cfg", BindMode.READ_ONLY)
								.withCommand("/bin/sh", "-c",
										"echo \"cfg_file=/opt/nagios/etc/objects/jmole.cfg\" >> /opt/nagios/etc/nagios.cfg;" +
										". /usr/local/bin/start_nagios;")
								.withAccessToHost(true)
								.withExposedPorts(80);

	static {
		Testcontainers.exposeHostPorts(5666);
	}

        @Test
        public void testProtocolMunin() throws Exception {
                try {
                        System.setProperty("jmole.protocol.nrpe.enabled", "true");
                        Lifecycle.setup();

                        LOG.log(Level.INFO, "This testcase will sleep for 3 min as it takes Nagios up to 2 min to collect values");
			Thread.sleep(60*1000*3);

			// Simple check status file
			Container.ExecResult result = container.execInContainer("/bin/cat", "/opt/nagios/var/status.dat");
                        assertEquals(0, result.getExitCode());
                        String data = result.getStdout();

			assertTrue(data.contains("plugin_output={Test={TestApp=TestValue: 100.0 > 80}}"));
			assertTrue(data.contains("plugin_output={Test={TestApp=TestValue: 100.0 > 39}}"));
                } finally {
                        Lifecycle.cleanup();
                        System.clearProperty("jmole.protocol.nrpe.enabled");
                }
        }

}
