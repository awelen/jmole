/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.wait.strategy.Wait;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.json.JSONException;

public abstract class AbstractTomcatTest extends AbstractWarningAndCriticalUsingJolokiaTest {

	static GenericContainer<?> createTomcatContainer(String name, String configFileName) {
		return new GenericContainer(name)
			.withFileSystemBind("target/configs/" + configFileName, "/tmp/JMole_Tomcat.xml", BindMode.READ_ONLY)
			.withFileSystemBind("target/dependencies/jmole-web.war", "/usr/local/tomcat/webapps/jmole-web.war", BindMode.READ_ONLY)
			.withFileSystemBind("target/dependencies/jolokia-war-unsecured.war", "/usr/local/tomcat/webapps/jolokia-war-unsecured.war", BindMode.READ_ONLY)
			.withEnv("CATALINA_OPTS", "-Djmole.config.level=5 -Djmole.config.filename.TOMCAT=/tmp/JMole_Tomcat.xml")
			.withExposedPorts(8080);
	}	

        static GenericContainer<?> createTomcatJakartaContainer(String name, String configFileName) {
                return new GenericContainer(name)
                        .withFileSystemBind("target/configs/" + configFileName, "/tmp/JMole_Tomcat.xml", BindMode.READ_ONLY)
                        .withFileSystemBind("target/dependencies/jmole-jakarta-web.war", "/usr/local/tomcat/webapps/jmole-web.war", BindMode.READ_ONLY)
                        .withFileSystemBind("target/dependencies/jolokia-agent-war-unsecured.war", "/usr/local/tomcat/webapps/jolokia-war-unsecured.war", BindMode.READ_ONLY)
                        .withEnv("CATALINA_OPTS", "-Djmole.config.level=5 -Djmole.config.filename.TOMCAT=/tmp/JMole_Tomcat.xml")
                        .withExposedPorts(8080);
        }


	static private String measurements = null;

	@Before
        public void collectMeasurements() throws IOException {
		if (measurements == null) {
			measurements = Utils.callRESTService("http://"+ getContainer().getHost() + ":" + getContainer().getMappedPort(8080) + "/jolokia-war-unsecured/exec/net.welen.jmole:service=jmole/collectMeasurements()");
		}
        }

	@Test
	public void testNetworkConnections() throws JSONException {
		try {
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Requests: \\\"http-nio-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		} catch (AssertionError e) {
			System.out.println("NIO not used trying APR");
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Requests: \\\"http-apr-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		}
		try {
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Connection pool: \\\"http-nio-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                } catch (AssertionError e) {
                        System.out.println("NIO not used trying APR");
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Connection pool: \\\"http-apr-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                }
		try {
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Network traffic: \\\"http-nio-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                } catch (AssertionError e) {
                        System.out.println("NIO not used trying APR");
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Network traffic: \\\"http-apr-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                }
		try {
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Thread pool: \\\"http-nio-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                } catch (AssertionError e) {
                        System.out.println("NIO not used trying APR");
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Thread pool: \\\"http-apr-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                }
	}

	@Test
        public void testServlet() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"Servlet\\/JSP\":[{\"Requests: \\/\\/localhost\\/jmole-web LifeCycleServlet\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"Servlet\\/JSP\":[{\"Instance allocations: \\/\\/localhost\\/jmole-web LifeCycleServlet\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"Servlet\\/JSP\":[{\"Execution time: \\/\\/localhost\\/jmole-web LifeCycleServlet\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

        @Test
        public void testCaches() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"Tomcat internal caches\":[{\"String cache\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"Tomcat internal caches\":[{\"Cache size: \\/jmole-web\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"Tomcat internal caches\":[{\"Cache hits: \\/jmole-web\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }


	abstract GenericContainer<?> getContainer();
}
