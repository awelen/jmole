/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.wait.strategy.Wait;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.json.JSONException;

public abstract class AbstractWildFlyTest extends AbstractWarningAndCriticalUsingJolokiaTest {

	static GenericContainer<?> createWildFlyContainer(String name, String configFileName) {
		String prefix = "/opt/jboss/wildfly";		// WildFly images frpm dockerhub
		if (name.contains("bitnami/wildfly")) {		// WildFly Images from quay.io
			prefix = "/opt/bitnami/wildfly";
		}
		return new GenericContainer(name)
			.withClasspathResourceMapping("wildfly/setup.cli", "/tmp/setup.cli", BindMode.READ_ONLY)
			.withFileSystemBind("target/configs/" + configFileName, "/tmp/JMole_WildFly.xml", BindMode.READ_ONLY)
			.withFileSystemBind("target/dependencies/jmole-ejb-jar-with-dependencies.jar", prefix + "/standalone/deployments/jmole-ejb-jar-with-dependencies.jar", BindMode.READ_ONLY)
			.withFileSystemBind("target/dependencies/jmole-jee-testapplication.war", prefix + "/standalone/deployments/jmole-jee-testapplication.war", BindMode.READ_ONLY)
			.withFileSystemBind("target/dependencies/jolokia-war-unsecured.war", prefix + "/standalone/deployments/jolokia-war-unsecured.war", BindMode.READ_ONLY)
			.withCommand("/bin/sh", "-c",
					prefix + "/bin/jboss-cli.sh --file=/tmp/setup.cli;" +
					prefix + "/bin/standalone.sh -b 0.0.0.0 -c standalone-full.xml")
			.withExposedPorts(8080)
			.waitingFor(Wait.forLogMessage(".*WildFly .* started in .*\\n", 1));
	}	

        static GenericContainer<?> createWildFlyJakartaContainer(String name, String configFileName) {
                String prefix = "/opt/jboss/wildfly";           // WildFly images frpm dockerhub
                if (name.contains("bitnami/wildfly")) {         // WildFly Images from quay.io
                        prefix = "/opt/bitnami/wildfly";
                }
                return new GenericContainer(name)
                        .withClasspathResourceMapping("wildfly/setup.cli", "/tmp/setup.cli", BindMode.READ_ONLY)
                        .withFileSystemBind("target/configs/" + configFileName, "/tmp/JMole_WildFly.xml", BindMode.READ_ONLY)
                        .withFileSystemBind("target/dependencies/jmole-jakarta-ejb-jar-with-dependencies.jar", prefix + "/standalone/deployments/jmole-jakarta-ejb-jar-with-dependencies.jar", BindMode.READ_ONLY)
                        .withFileSystemBind("target/dependencies/jmole-jakarta-ee-testapplication.war", prefix + "/standalone/deployments/jmole-jee-testapplication.war", BindMode.READ_ONLY)
                        .withFileSystemBind("target/dependencies/jolokia-agent-war-unsecured.war", prefix + "/standalone/deployments/jolokia-war-unsecured.war", BindMode.READ_ONLY)
                        .withCommand("/bin/sh", "-c",
                                        prefix + "/bin/jboss-cli.sh --file=/tmp/setup.cli;" +
                                        prefix + "/bin/standalone.sh -b 0.0.0.0 -c standalone-full.xml")
                        .withExposedPorts(8080)
                        .waitingFor(Wait.forLogMessage(".*WildFly .* started in .*\\n", 1));
        }


	static private String measurements = null;

	@Before
        public void collectMeasurements() throws IOException {
		if (measurements == null) {
			measurements = Utils.callRESTService("http://"+ getContainer().getHost() + ":" + getContainer().getMappedPort(8080) + "/jolokia-war-unsecured/exec/net.welen.jmole:service=jmole/collectMeasurements()");
		}
        }

	@Test
	public void testJMS() throws JSONException {
		JSONAssert.assertEquals("{\"value\":{\"JMS\":[{\"Queue: testQueue\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"JMS\":[{\"Topic: testTopic\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"JMS\":[{\"Queue: DLQ\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"JMS\":[{\"Queue: ExpiryQueue\":{}}]}}", measurements, JSONCompareMode.LENIENT);
	}

	@Test
        public void testDatasource() throws JSONException {
		JSONAssert.assertEquals("{\"value\":{\"Datasources\":[{\"Prepared Statement Cache: testDS\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"Datasources\":[{\"Connection pool time value: testDS\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"Datasources\":[{\"Connection pool: testDS\":{}}]}}", measurements, JSONCompareMode.LENIENT);
	}

	@Test
        public void testJTA() throws JSONException {
		JSONAssert.assertEquals("{\"value\":{\"JTA\":[{\"JTA Transactions\":{}}]}}", measurements, JSONCompareMode.LENIENT);
	}

        @Test
        public void testWeb() throws JSONException {
		JSONAssert.assertEquals("{\"value\":{\"WEB\":[{\"Servlet Request Count: jmole-jee-testapplication.war null TestServlet\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"WEB\":[{\"Servlet Request time: jmole-jee-testapplication.war null TestServlet\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }


	@Test
        public void testEJB() throws JSONException {
		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateless Session Bean Instance Pool: jmole-jee-testapplication.war null SLSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateless Session Bean Invocations: jmole-jee-testapplication.war null SLSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateless Session Bean Execution Time: jmole-jee-testapplication.war null SLSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);


		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateful Session Bean: jmole-jee-testapplication.war null SFSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateful Session Bean Execution Time: jmole-jee-testapplication.war null SFSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);

		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Message Driven Bean Execution Time: jmole-jee-testapplication.war null QueueMDB\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Message Driven Instance Pool: jmole-jee-testapplication.war null QueueMDB\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Message Driven Instance Pool: jmole-jee-testapplication.war null TopicMDB\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Message Driven Bean Execution Time: jmole-jee-testapplication.war null TopicMDB\":{}}]}}", measurements, JSONCompareMode.LENIENT);

		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Singleton Bean Invocations: jmole-jee-testapplication.war null StartBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Singleton Bean Execution Time: jmole-jee-testapplication.war null StartBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);

		JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"EJB3 Thread Pool: default\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

	abstract GenericContainer<?> getContainer();
}
