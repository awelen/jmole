/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;
import java.net.*;
import java.io.*;

public class Utils {

        static public String callRESTService(String urlString) throws IOException {
		InputStream inputStream = null;
		Reader reader = null;
		try {
			URL url = new URL(urlString);
		       	HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestProperty("accept", "application/json");
			inputStream = con.getInputStream();

			StringBuilder sb = new StringBuilder();
			reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(StandardCharsets.UTF_8.name())));
			int c = 0;
			while ((c = reader.read()) != -1) {
				sb.append((char) c);
			}
			return sb.toString();
		} finally {
			if (reader != null) {
                                reader.close();
                        }
			if (inputStream != null) {
				inputStream.close();
			}
		}
	}

}
