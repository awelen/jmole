/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.wait.strategy.Wait;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.json.JSONException;

public abstract class AbstractTomEETest extends AbstractWarningAndCriticalUsingJolokiaTest {

	static GenericContainer<?> createTomEEContainer(String name, String configFileName) {
		return new GenericContainer(name)
			.withFileSystemBind("target/configs/" + configFileName, "/tmp/JMole_TomEE.xml", BindMode.READ_ONLY)
			.withFileSystemBind("target/dependencies/jmole-web.war", "/usr/local/tomee/webapps/jmole-web.war", BindMode.READ_ONLY)
			.withFileSystemBind("target/dependencies/jolokia-war-unsecured.war", "/usr/local/tomee/webapps/jolokia-war-unsecured.war", BindMode.READ_ONLY)
			.withFileSystemBind("target/dependencies/jmole-jee-testapplication.war", "/usr/local/tomee/webapps/jmole-jee-testapplication.war", BindMode.READ_ONLY)
			.withEnv("CATALINA_OPTS", "-Djmole.config.level=5 -Djmole.config.filename.TOMEE=/tmp/JMole_TomEE.xml")
			.withExposedPorts(8080);
	}	

        static GenericContainer<?> createTomEEJakartaContainer(String name, String configFileName) {
                return new GenericContainer(name)
                        .withFileSystemBind("target/configs/" + configFileName, "/tmp/JMole_TomEE.xml", BindMode.READ_ONLY)
                        .withFileSystemBind("target/dependencies/jmole-jakarta-web.war", "/usr/local/tomee/webapps/jmole-web.war", BindMode.READ_ONLY)
                        .withFileSystemBind("target/dependencies/jolokia-agent-war-unsecured.war", "/usr/local/tomee/webapps/jolokia-war-unsecured.war", BindMode.READ_ONLY)
                        .withFileSystemBind("target/dependencies/jmole-jakarta-ee-testapplication.war", "/usr/local/tomee/webapps/jmole-jee-testapplication.war", BindMode.READ_ONLY)
                        .withEnv("CATALINA_OPTS", "-Djmole.config.level=5 -Djmole.config.filename.TOMEE=/tmp/JMole_TomEE.xml")
                        .withExposedPorts(8080);
        }


	static private String measurements = null;

	@Before
        public void collectMeasurements() throws IOException, InterruptedException  {

		// Sleep for more than 10 sec to be sure that the lazy kick-in of the batch in the test application kicks in
		Thread.sleep(15000);

		if (measurements == null) {
			measurements = Utils.callRESTService("http://"+ getContainer().getHost() + ":" + getContainer().getMappedPort(8080) + "/jolokia-war-unsecured/exec/net.welen.jmole:service=jmole/collectMeasurements()");
		}
        }

	//
	// Tests from Tomcat test suite
	//
	@Test
	public void testNetworkConnections() throws JSONException {
		try {
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Requests: \\\"http-nio-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		} catch (AssertionError e) {
			System.out.println("NIO not used trying APR");
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Requests: \\\"http-apr-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
		}
		try {
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Connection pool: \\\"http-nio-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                } catch (AssertionError e) {
                        System.out.println("NIO not used trying APR");
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Connection pool: \\\"http-apr-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                }
		try {
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Network traffic: \\\"http-nio-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                } catch (AssertionError e) {
                        System.out.println("NIO not used trying APR");
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Network traffic: \\\"http-apr-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                }
		try {
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Thread pool: \\\"http-nio-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                } catch (AssertionError e) {
                        System.out.println("NIO not used trying APR");
			JSONAssert.assertEquals("{\"value\":{\"Network Connections\":[{\"Thread pool: \\\"http-apr-8080\\\"\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                }
	}

	@Test
        public void testServlet() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"Servlet\\/JSP\":[{\"Requests: \\/\\/localhost\\/jmole-web LifeCycleServlet\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"Servlet\\/JSP\":[{\"Instance allocations: \\/\\/localhost\\/jmole-web LifeCycleServlet\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"Servlet\\/JSP\":[{\"Execution time: \\/\\/localhost\\/jmole-web LifeCycleServlet\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

        @Test
        public void testCaches() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"Tomcat internal caches\":[{\"String cache\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"Tomcat internal caches\":[{\"Cache size: \\/jmole-web\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"Tomcat internal caches\":[{\"Cache hits: \\/jmole-web\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

	// End of Tomcat tests

	@Test
	public void testActiveMQ() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"ActiveMQ Broker\":[{\"Overall connection information for broker: localhost\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"ActiveMQ Broker\":[{\"Storage limits: localhost\":{}}]}}", measurements, JSONCompareMode.LENIENT);
	}

	@Test
	public void testJMS() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"JMS\":[{\"Destination: localhost Queue jmole-jee-testapplication\\/jms\\/testQueue\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"JMS\":[{\"Destination message sizes: localhost Queue jmole-jee-testapplication\\/jms\\/testQueue\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"JMS\":[{\"Destination storage limits: localhost Queue jmole-jee-testapplication\\/jms\\/testQueue\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"JMS\":[{\"Destination time values: localhost Queue jmole-jee-testapplication\\/jms\\/testQueue\":{}}]}}", measurements, JSONCompareMode.LENIENT);
	}

	@Test
	public void testJTA() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"JTA\":[{\"JTA Transactions\":{}}]}}", measurements, JSONCompareMode.LENIENT);
	}

	@Test
        public void testDatasources() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"Datasources\":[{\"Connection pool: jmole-jee-testapplication_testDS\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

	@Test
        public void testEJB() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Singleton Bean Invocations: <empty> file_\\/usr\\/local\\/tomee\\/webapps\\/jmole-jee-testapplication\\/ StartBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Singleton Bean Execution Time: <empty> file_\\/usr\\/local\\/tomee\\/webapps\\/jmole-jee-testapplication\\/ StartBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateless Session Bean Invocations: <empty> file_\\/usr\\/local\\/tomee\\/webapps\\/jmole-jee-testapplication\\/ SLSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateless Session Bean Execution Time: <empty> file_\\/usr\\/local\\/tomee\\/webapps\\/jmole-jee-testapplication\\/ SLSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateless Session Bean Instance Pool: <empty> file_\\/usr\\/local\\/tomee\\/webapps\\/jmole-jee-testapplication\\/ SLSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateful Session Bean: <empty> file_\\/usr\\/local\\/tomee\\/webapps\\/jmole-jee-testapplication\\/ SFSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
                JSONAssert.assertEquals("{\"value\":{\"EJB3\":[{\"Stateful Session Bean Execution Time: <empty> file_\\/usr\\/local\\/tomee\\/webapps\\/jmole-jee-testapplication\\/ SFSBBean\":{}}]}}", measurements, JSONCompareMode.LENIENT);
	}

	abstract GenericContainer<?> getContainer();
}
