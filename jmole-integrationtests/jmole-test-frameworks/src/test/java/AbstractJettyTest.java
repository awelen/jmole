/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.wait.strategy.Wait;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.json.JSONException;

public abstract class AbstractJettyTest extends AbstractWarningAndCriticalUsingJolokiaTest {

        static GenericContainer<?> createJettyContainer(String name, String configFileName) {
		return new GenericContainer(name)
                .withFileSystemBind("target/configs/" + configFileName, "/tmp/JMole_Jetty.xml", BindMode.READ_ONLY)
                .withFileSystemBind("target/dependencies/jmole-web.war", "/var/lib/jetty/webapps/jmole-web.war", BindMode.READ_ONLY)
                .withFileSystemBind("target/dependencies/jolokia-war-unsecured.war", "/var/lib/jetty/webapps/jolokia-war-unsecured.war", BindMode.READ_ONLY)
                .withCommand("/bin/sh", "-c",
                                "java -jar $JETTY_HOME/start.jar --add-to-start=jmx,stats;" +
                                "java -jar -Djmole.config.level=5 -Djmole.config.filename.JETTY=/tmp/JMole_Jetty.xml $JETTY_HOME/start.jar")
                .withExposedPorts(8080);
        }

	static GenericContainer<?> createJakartaJettyContainer(String name, String configFileName) {
                return new GenericContainer(name)
                .withFileSystemBind("target/configs/" + configFileName, "/tmp/JMole_Jetty.xml", BindMode.READ_ONLY)
                .withFileSystemBind("target/dependencies/jmole-jakarta-web.war", "/var/lib/jetty/webapps/jmole-web.war", BindMode.READ_ONLY)
                .withFileSystemBind("target/dependencies/jolokia-agent-war-unsecured.war", "/var/lib/jetty/webapps/jolokia-war-unsecured.war", BindMode.READ_ONLY)
                .withCommand("/bin/sh", "-c",
                                "java -jar $JETTY_HOME/start.jar --add-to-start=jmx,stats;" +
                                "java -jar -Djmole.config.level=5 -Djmole.config.filename.JETTY=/tmp/JMole_Jetty.xml $JETTY_HOME/start.jar")
                .withExposedPorts(8080);
        }

        private static String measurements = null;

        @Before
        public void collectMeasurements() throws IOException {
		if (measurements == null) {
			measurements = Utils.callRESTService("http://"+ getContainer().getHost() + ":" + getContainer().getMappedPort(8080) + "/jolokia-war-unsecured/exec/net.welen.jmole:service=jmole/collectMeasurements()");
		}
        }

        @Test
        public void testRequestStatistics() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"WEB\":[{\"Request Statistics (Server id: 0)\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

        @Test
        public void testResponseCodes() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"WEB\":[{\"Response Codes (Server id: 0)\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

        @Test
        public void testTimeStatistics() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"WEB\":[{\"Time Statistics (Server id: 0)\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

        @Test
        public void testThreads() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"WEB\":[{\"Threads (Server id: 0)\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

        @Test
        public void testSessions() throws JSONException {
                JSONAssert.assertEquals("{\"value\":{\"WEB\":[{\"Sessions: 0 jmole-web\":{}}]}}", measurements, JSONCompareMode.LENIENT);
        }

	abstract GenericContainer<?> getContainer();
}
