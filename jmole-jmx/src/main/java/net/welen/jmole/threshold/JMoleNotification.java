package net.welen.jmole.threshold;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.management.Notification;
import javax.management.ObjectName;

public class JMoleNotification extends Notification {

	public static String TYPE_WARNING = "net.welen.jmole.warning";
	public static String TYPE_CRITICAL = "net.welen.jmole.critical";

	public JMoleNotification(String type, ObjectName objectName, long seqNum, String msg) {
                super(type, objectName, seqNum, msg);
	}

}
