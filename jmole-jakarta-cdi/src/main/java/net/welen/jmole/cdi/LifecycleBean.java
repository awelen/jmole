package net.welen.jmole.cdi;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Destroyed;
import jakarta.enterprise.context.Initialized;
import jakarta.enterprise.event.Event;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationFilterSupport;
import javax.management.NotificationListener;
import javax.management.ObjectName;

import net.welen.jmole.JMole;
import net.welen.jmole.Lifecycle;
import net.welen.jmole.Utils;

/**
 * A CDI component that is used to handle the lifecycle of JMole.
 */
@ApplicationScoped
public class LifecycleBean implements NotificationListener {

	private final static Logger LOG = Logger.getLogger(LifecycleBean.class.getName());
	
	private MBeanServer server = Utils.getMBeanServer();
	
	@Inject
	@JMoleStarted
	private Event<String> jmoleStarted;

	@Inject
	@JMoleStopped
	private Event<String> jmoleStopped;

	@Inject
	@JMoleReconfigured
	private Event<String> jmoleReconfigured;

	// JMX Listener
	private static ObjectName listenerName = null;
	private static LifecycleBean listener = null;

	@SuppressWarnings("unused")
	private void setup(@Observes @Initialized(ApplicationScoped.class) Object init) {
		LOG.log(Level.FINE, "Starting JMole");
		Lifecycle.setup();

		LOG.log(Level.FINE, "Registering JMX listener");
		try {			
			listenerName = new ObjectName(JMole.OBJECT_NAME);
			listener = this;			
			NotificationFilterSupport filter = new NotificationFilterSupport();
			filter.enableType(JMole.NOTIFICATION_TYPE);			
			server.addNotificationListener(listenerName, listener, filter, null);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Failed to add JMX NotificationListener: " + e.getMessage(), e);
		}
		
		LOG.log(Level.FINE, "Sending CDI event");
		jmoleStarted.fire("JMole Started");		
	}

	@SuppressWarnings("unused")
	private void teardown(@Observes @Destroyed(ApplicationScoped.class) Object init) {
		LOG.log(Level.FINE, "Removing JMX listener");		
		try {		
			server.removeNotificationListener(listenerName, listener);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Failed to remove JMX NotificationListener: " + e.getMessage(), e);
		}
		
		LOG.log(Level.FINE, "Stopping JMole");
		Lifecycle.cleanup();
		
		LOG.log(Level.FINE, "Sending CDI event");
		jmoleStopped.fire("JMole Stopped");
	}
	
	public JMole getJMoleInstance() {
		return Lifecycle.getJMoleInstance();
	}
	
	public void handleNotification(Notification notification, Object handback) {
		LOG.log(Level.FINE, "MBean notification recieved: " + notification + ". Sending CDI event");
		jmoleReconfigured.fire(notification.getMessage());		
	}

}
