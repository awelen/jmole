package net.welen.jmole.threshold;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.ObjectName;
import javax.management.Notification;
import javax.management.MalformedObjectNameException;

import net.welen.jmole.JMole;
import net.welen.jmole.Configuration;
import net.welen.jmole.collector.MBeanCollector;
import net.welen.jmole.presentation.PresentationInformation;

public class ThresholdThread extends Thread {
	
	private final static Logger LOG = Logger.getLogger(ThresholdThread.class.getName());
	private static final Object sequenceNumberLock = new Object();
	private static long sequenceNumber = 0;
		
	private boolean stopped = false;
	private long interval = 60000L;

	private JMole jmole;

	private Map<ObjectName, String> warningMessages = new HashMap<ObjectName, String>();
	private Map<ObjectName, String> criticalMessages = new HashMap<ObjectName, String>();

	private Map<ObjectName, Configuration> configurations = new HashMap<ObjectName, Configuration>();

	public ThresholdThread(String name, long interval, JMole jmole) {
		super(name);
		this.interval = interval;
		this.jmole = jmole;
	}

	@Override
	public void run() {
		stopped = false;
		while (!stopped) {		
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				LOG.log(Level.FINE, e.getMessage(), e);
			}

			LOG.log(Level.FINE, "Running threshold check");

			Map<ObjectName, String> newWarningMessages = new HashMap<ObjectName, String>();
			Map<ObjectName, String> newCriticalMessages = new HashMap<ObjectName, String>();

			// Match thresholds with this thread's "interval"
			for (Configuration configuration : jmole.getConfiguration()) {
				for (Entry<String, Threshold> entry : configuration.getThresholds().entrySet()) {
					String attribute = entry.getKey();
					Threshold threshold = entry.getValue();

					// Correct "interval"?
					if (threshold.getInterval() == interval) {
					
						// Check all matching MBeans values
						try {
							for (ObjectName objectName : configuration.getMBeanFinder().getMatchingObjectNames()) {

								configurations.put(objectName, configuration);

								try {
									if (stopped) {
										return;
									}

									MBeanCollector mBeanCollector = configuration.getMBeanCollector();
									Object valueObject = mBeanCollector.getValues(objectName).get(attribute);
									if (valueObject == null) {
										LOG.log(Level.FINE, "Data collection returned null. Skipping it as it is probably not calculated yet");
										continue;
									}
									
									Double value = null;
									if (valueObject instanceof Boolean) {
										Boolean b = (Boolean) valueObject;
										if (b) {
											value = 1d;
										} else {
											value = 0d;
										}
									} else {
										value = Double.parseDouble(valueObject.toString());
									}
					

									ThresholdValues individualThresholdValue = threshold.getIndividualThresholds().get(attribute);
									String low;
									String high;
					
									String message = threshold.getMessage();
					
									// Warning
									if (individualThresholdValue == null) {
										low = threshold.getWarningLowThreshold();	
										high = threshold.getWarningHighThreshold();
									} else {
										low = individualThresholdValue.getWarningLowThreshold();
										high = individualThresholdValue.getWarningHighThreshold();
										if (individualThresholdValue.getMessage() != null) {
											message = individualThresholdValue.getMessage();
										}
									}
									low = Threshold.calculateThreshold(low, mBeanCollector, objectName, attribute);
									high = Threshold.calculateThreshold(high, mBeanCollector, objectName, attribute);

									PresentationInformation presentationInformation = configuration.getPresentationInformation();
									String name = configuration.getMBeanCollector().getConstructedName(objectName);
									String label = presentationInformation.translateAttributeLabel(attribute);

									if (!low.isEmpty() && value < Double.parseDouble(low)) {
										String messageText = constructMessage(message, label + ": " + value + " < " + low);
										LOG.log(Level.FINE, "Threshold trigged: " + messageText);
										sendJMXNotification(false, attribute, name, presentationInformation, messageText);
										newWarningMessages.put(objectName, messageText);
									}
									if (!high.isEmpty() && value > Double.parseDouble(high)) {
										String messageText = constructMessage(message, label + ": " + value + " > " + high);
										LOG.log(Level.FINE, "Threshold trigged: " + messageText);
										sendJMXNotification(false, attribute, name, presentationInformation, messageText);
										newWarningMessages.put(objectName, messageText);
									}

									message = threshold.getMessage();
						
									// Critical					
									if (individualThresholdValue == null) {
										low = threshold.getCriticalLowThreshold();	
										high = threshold.getCriticalHighThreshold();
									} else {
										low = individualThresholdValue.getCriticalLowThreshold();
										high = individualThresholdValue.getCriticalHighThreshold();
										if (individualThresholdValue.getMessage() != null) {
											message = individualThresholdValue.getMessage();
										}
									}
									low = Threshold.calculateThreshold(low, mBeanCollector, objectName, attribute);
									high = threshold.calculateThreshold(high, mBeanCollector, objectName, attribute);
					
									if (!low.isEmpty() && value < Double.parseDouble(low)) {
										String messageText = constructMessage(message, label + ": " + value + " < " + low);
										LOG.log(Level.FINE, "Threshold trigged: " + messageText);
										sendJMXNotification(true, attribute, name, presentationInformation, messageText);
										newCriticalMessages.put(objectName, messageText);
									}
									if (!high.isEmpty() && value > Double.parseDouble(high)) {
										String messageText = constructMessage(message, label + ": " + value + " > " + high);
										LOG.log(Level.FINE, "Threshold trigged: " + messageText);
										sendJMXNotification(true, attribute, name, presentationInformation, messageText);
										newCriticalMessages.put(objectName, messageText);
									}

								} catch (Throwable t) {
									LOG.log(Level.SEVERE, t.getMessage(), t);
								}
							}
						} catch (Exception e) {
							LOG.log(Level.SEVERE, e.getMessage(), e);
						}

						warningMessages = newWarningMessages;
						criticalMessages = newCriticalMessages;
					}			
				}
			}
		}

		LOG.log(Level.FINE, "Thread stopped");
	}

	private void sendJMXNotification(boolean critical, String attribute, String name, PresentationInformation presentationInformation, String message) {
                String type = JMoleNotification.TYPE_WARNING;
                if (critical) {
                        type = JMoleNotification.TYPE_CRITICAL;
                }
		synchronized(sequenceNumberLock) {
			try {
				Notification n = new JMoleNotification(type, new ObjectName(JMole.OBJECT_NAME), sequenceNumber++, message);
				n.setUserData(new NotificationUserData(name, attribute, presentationInformation));
				LOG.log(Level.FINE, "Sending notification: " + n);
				jmole.sendNotification(n);
			} catch (MalformedObjectNameException e) {
				LOG.log(Level.SEVERE, "Couldn't send JMX notification: " + e.getMessage(), e);
			}
		}
	}

	private String constructMessage(String message, String problemText) {
		return String.format(message, problemText);
	}

	public Configuration getConfiguration(ObjectName objectName) {
		return configurations.get(objectName);
	}
	
	public void stopThread() {
		LOG.log(Level.FINE, "Stopping thread");
		stopped = true;
	}

	public Map<ObjectName, String> getWarningMessages() {
		return warningMessages;
	}

	public Map<ObjectName, String> getCriticalMessages() {
		return criticalMessages;
	}
	
}
