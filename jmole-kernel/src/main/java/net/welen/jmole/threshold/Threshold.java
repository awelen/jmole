package net.welen.jmole.threshold;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;
import javax.management.AttributeNotFoundException;
import javax.management.MBeanException;

import net.welen.jmole.Utils;
import net.welen.jmole.collector.MBeanCollector;

import javax.management.ObjectName;

public class Threshold {
	
	private final static Logger LOG = Logger.getLogger(Threshold.class.getName());
		
	private long interval = 60000L;

	private ThresholdValues thresholdValues = new ThresholdValues();
	private Map<String, ThresholdValues> individualThresholdValues = new HashMap<String, ThresholdValues> ();

	private String attribute;
	private String label;
	private String message = "%s";

	
	public String getCriticalLowThreshold() {
		return thresholdValues.getCriticalLowThreshold();
	}

	public void setCriticalLowThreshold(String criticalLowThreshold) {
		this.thresholdValues.setCriticalLowThreshold(criticalLowThreshold);
	}

	public String getCriticalHighThreshold() {
		return thresholdValues.getCriticalHighThreshold();
	}

	public void setCriticalHighThreshold(String criticalHighThreshold) {
		this.thresholdValues.setCriticalHighThreshold(criticalHighThreshold);
	}		

	public String getWarningLowThreshold() {
		return thresholdValues.getWarningLowThreshold();
	}

	public void setWarningLowThreshold(String warningLowThreshold) {
		this.thresholdValues.setWarningLowThreshold(warningLowThreshold);
	}

	public String getWarningHighThreshold() {
		return thresholdValues.getWarningHighThreshold();
	}

	public void setWarningHighThreshold(String warningHighThreshold) {
		this.thresholdValues.setWarningHighThreshold(warningHighThreshold);
	}		

	public void setIndividualThresholds(Map<String, ThresholdValues> individualThresholdValues) {
		this.individualThresholdValues = individualThresholdValues;
	}

	public Map<String, ThresholdValues> getIndividualThresholds() {
		return individualThresholdValues;
	}
	
	public long getInterval() {
		return interval;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		if (label != null) {
			return label;
		} 
		return attribute;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

        static public String calculateThreshold(String thresholdString, MBeanCollector mbeanCollector, ObjectName objectName, String attribute) throws InstanceNotFoundException, ReflectionException, AttributeNotFoundException, MBeanException {
                String data = thresholdString;
                Map<String, Object> values = mbeanCollector.getValues(objectName);
                LOG.log(Level.FINE, "Before variable replacement: " + data);
                for (String attributeName : mbeanCollector.getAttributes()) {
                        if (values.containsKey(attributeName) && values.get(attributeName) != null) {
                                String tmpSplit[] = data.split(",");
                                for (int i=0; i<tmpSplit.length; i++) {
                                        if (tmpSplit[i].equals(attributeName)) {
                                                tmpSplit[i] = values.get(attributeName).toString();
                                        }
                                }
                                StringBuffer tmp = new StringBuffer();
                                for (String part : tmpSplit) {
                                        if (tmp.length() > 0) {
                                                tmp.append(",");
                                        }
                                        tmp.append(part);
                                }
                                data = tmp.toString();
                        }
                }
                LOG.log(Level.FINE, "After variable replacement: " + data);
                return Utils.rpnCalculate(data);
        }

}
