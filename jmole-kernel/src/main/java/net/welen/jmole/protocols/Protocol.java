package net.welen.jmole.protocols;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.management.NotificationListener;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;

public interface Protocol extends NotificationListener {
			
	/**
	 * Callback for starting the protocol
	 * 
	 * @throws Exception
	 */
	void startProtocol(JMole jmole) throws Exception;

	/**
	 * Callback for stopping the protocol
	 * 
	 * @throws Exception
	 */
	void stopProtocol() throws Exception;
	
	/**
	 * Set protocol name 
	 */
	void setName(String name);
	
	/**
	 * Get the name of the protocol
	 */
	String getName();

	/**
	 * Get the JMole instance
	 */
	JMole getJMole();

	/**
	 * Handle Warning
	 */	
	void handleWarning(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception;

	/**
         * Handle critical problem
         */
	void handleCritical(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception;
	
}
