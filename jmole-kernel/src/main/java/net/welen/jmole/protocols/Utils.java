package net.welen.jmole.protocols;

import java.math.BigDecimal;
import java.util.logging.Level;

import net.welen.jmole.presentation.PresentationInformation;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

public class Utils {
	
	private final static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(Utils.class.getName());
	
	/**
	 * 
	 * @param format
	 * @param sep
	 * @param categoryEntry
	 * @param nameEntry
	 * @param attributeEntry
	 * @param presentationInformation
	 *
	 * @throws NumberFormatException
	 * 
	 * @return
	 */
	public static String formatLogString(String format, char sep, String category, String name, String attribute, Object value, PresentationInformation presentationInformation) {

		String answer = format;

		// %c
                answer = answer.replace("%c", category);
		// %n
                answer = answer.replace("%n", name);
		// %t
                answer = answer.replace("%t", attribute);
		// %k
		String key = String.format("%s%c%s%c%s", category, sep, name, sep, attribute);
		answer = answer.replace("%k", key);
		// %a
		answer = answer.replace("%a", presentationInformation.translateAttributeLabel(attribute));
		// %A
		String attributeDescription = presentationInformation.getAttributeDescription(attribute);
		answer = answer.replace("%A", attributeDescription != null ? attributeDescription : "");
		// %v
		try {
			answer = answer.replace("%v", new BigDecimal(value.toString()).toPlainString());
		} catch (NumberFormatException e) {
			LOG.log(Level.FINE, e.getMessage() + ": " + key, e);
			answer = answer.replace("%v", value.toString());			
		}						
		
		// %U
		answer = answer.replace("%U", presentationInformation.getUnit());

		return answer;
	}
	
}
