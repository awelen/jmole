package net.welen.jmole.protocols.logger;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.protocols.AbstractIntervalProtocol;
import net.welen.jmole.protocols.Utils;
import net.welen.jmole.protocols.ProtocolMessagePlaceholder;

public class Logger extends AbstractIntervalProtocol implements LoggerMBean {

	private final static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(Logger.class.getName());

	private static final char SEP = '/';

	private static String PROPERTY_LOGGER_NAME = "jmole.protocol.logger.name";
	private static String PROPERTY_LOGGER_FORMAT = "jmole.protocol.logger.format";
	private static String PROPERTY_LOGGER_LEVEL = "jmole.protocol.logger.level";
	private static String PROPERTY_LOGGER_INTERVAL = "jmole.protocol.logger.interval";

        private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();

	private Level level;
	private String format;
	private java.util.logging.Logger protocolLogger = java.util.logging.Logger.getLogger("JMole");

	@Override
	public void startProtocol(JMole jmole) throws Exception {
		super.startProtocol(jmole);

		String levelString = System.getProperty(PROPERTY_LOGGER_LEVEL);
		if (levelString == null) {
			levelString = "INFO";
		}
		level = Level.parse(levelString);

		String name = System.getProperty(PROPERTY_LOGGER_NAME);
		if (name != null) {
			protocolLogger = java.util.logging.Logger.getLogger(name);
		}

		format = System.getProperty(PROPERTY_LOGGER_FORMAT);
		if (format == null) {
			format = "[%k] %a [%A] = %v %U";
		}
		
		Long interval = Long.getLong(PROPERTY_LOGGER_INTERVAL);
		if (interval != null) {
			setInterval(interval);
		}

		LOG.log(Level.INFO, "JMole Logger protocol started: Level=" + levelString + ", Interval=" + interval);
	}

	@Override
	public void stopProtocol() throws Exception {
		LOG.log(Level.INFO, "Stopping JMole Logger protocol");
		super.stopProtocol();
		LOG.log(Level.INFO, "JMole Logger protocol stopped");
	}

	@Override
	public String getLevel() {
		return level.getName();
	}

	@Override
	public void setLevel(String level) {
		this.level = Level.parse(level);
	}

	@Override
	public String getFormat() {
		return format;
	}

	@Override
	public void setFormat(String format) {
		this.format = format;
	}

	@Override
	protected void handleMeasurement(String category, String name, String attribute, Object value, PresentationInformation presentationInformation) throws Exception {
		if (getFormat().toUpperCase().equals("JSON")) {
			protocolLogger.log(level, gson.toJson(new ProtocolMessagePlaceholder("metric", new Date(), null, category, name, attribute, value, presentationInformation)));
		} else {
			protocolLogger.log(level, Utils.formatLogString(format, SEP, category, name, attribute, value, presentationInformation));		
		}
	}

	@Override
	public void handleWarning(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		if (getFormat().toUpperCase().equals("JSON")) {
			protocolLogger.log(Level.WARNING, gson.toJson(new ProtocolMessagePlaceholder("warning", new Date(), message, presentationInformation.getCategory(), name, attribute, null, presentationInformation)));
		} else {
			protocolLogger.log(Level.WARNING, Utils.formatLogString(format, SEP, presentationInformation.getCategory(), name, attribute, message, presentationInformation));
		}
	}

	@Override
        public void handleCritical(String name, String attribute,PresentationInformation presentationInformation, String message) throws Exception {
		if (getFormat().toUpperCase().equals("JSON")) {
			protocolLogger.log(Level.SEVERE, gson.toJson(new ProtocolMessagePlaceholder("critical", new Date(), message, presentationInformation.getCategory(), name, attribute, null, presentationInformation)));
		} else {
			protocolLogger.log(Level.SEVERE, Utils.formatLogString(format, SEP, presentationInformation.getCategory(), name, attribute, message, presentationInformation));
		}
	}

}
