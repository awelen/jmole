package net.welen.jmole.protocols;

/*-
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import net.welen.jmole.presentation.PresentationInformation;

@SuppressWarnings("unused")
public class ProtocolMessagePlaceholder {
	private String type;
	private Date timestamp;
	private String message;
	private String category;
	private String name;
	private String attribute;
	private Object value;
	private String unit;
	private String label;
	
	private String description;

	public ProtocolMessagePlaceholder(String type, Date timestamp, String message, String category, String name, String attribute, Object value, PresentationInformation presentationInformation) {
		this.type = type;
		this.timestamp = timestamp;
		this.message = message;
		this.category = category;
		this.name = name;
		this.attribute = attribute;
		this.value = value;
		
		this.unit = presentationInformation.getUnit();
		this.label = presentationInformation.getAttributeLabel(attribute);
		this.description = presentationInformation.getAttributeDescription(attribute);		
	}

}
