package net.welen.jmole.protocols.munin;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;

import net.welen.jmole.Configuration;
import net.welen.jmole.collector.MBeanCollector;
import net.welen.jmole.finder.MBeanFinder;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.threshold.Threshold;
import net.welen.jmole.threshold.ThresholdValues;

public class MuninSocketHandler extends Thread {
	
	private final static Logger LOG = Logger.getLogger(MuninSocketHandler.class.getName());

	private Socket socket;
	private Munin setup;

	public MuninSocketHandler(Socket socket, Munin setup) {
		this.socket = socket;
		this.setup = setup;
		this.setName("JMole Munin protocol thread #" + setup.currentThreads);
	}

	public void run() {		
		try {
			setup.currentThreads++;
			socket.setSoTimeout(setup.getTcpReadTimeOut());
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
			PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"), true);
			boolean multigraph = false;
			boolean dirtyconfig = false;
			
			// Send greeting
			String nodeName = setup.getNodeName();
			if (nodeName == null) {
				nodeName = socket.getLocalAddress().getHostAddress();
			}
			String greeting ="# munin node at " + nodeName;
			LOG.log(Level.FINE, "Sent greeting: " + greeting);
			out.println(greeting);

			int maxThreads = setup.getMaxThreads();
			if (setup.currentThreads > maxThreads) {
				out.println("# Max number of clients reached: " + maxThreads + ". Hanging up.");
				throw new IOException("Max number of clients reached: " + maxThreads);
			}
			while (true) {
				String command = reader.readLine();
				if (command == null) {
					break;
				}

				// Strip/cleanup command
				command = command.replaceAll("\\b\\s{2,}\\b", " ").trim().toLowerCase(Locale.ENGLISH);

				LOG.log(Level.FINE, "Incoming command: " + command);
				
				// Parse command
				StringBuffer answer = new StringBuffer();
				if (command.equals("quit") || command.equals(".")) {	// quit
					socket.close();
					return;
				} else if (command.equals("version")) {			// version				
					answer.append("JMole Munin node");
				} else if (command.equals("nodes")) {			// nodes
					answer.append(nodeName + "\n.");
				} else if (command.startsWith("list")) {		// list					
					if (multigraph) {
						answer.append("jmole");
					} else {
						answer.append(getList());
					}
				} else if (command.startsWith("cap")) {			// cap				
					if (command.contains("multigraph")) {
						multigraph = true;						
					}
					if (command.contains("dirtyconfig")) {
						dirtyconfig = true;						
					}
					answer.append("cap multigraph dirtyconfig");
				} else if (command.startsWith("config ")) {		// config
					if (multigraph && command.endsWith(" jmole")) {
						answer.append(executeMultigraphConfig(dirtyconfig, multigraph));
					} else {
						String part = command.replace("config ", "");
						answer.append(executeConfig(part, dirtyconfig, multigraph));
					}
				} else if (command.startsWith("fetch ")) {		// fetch
					if (multigraph && command.endsWith(" jmole")) {
						answer.append(executeMultigraphFetch());
					} else {
						String part = command.replace("fetch ", "");
						answer.append(executeFetch(part));
					}	
				} else {
					LOG.log(Level.WARNING, "Unknown command: " + command);
					answer.append("# Unknown command");
				}
				
				LOG.log(Level.FINE, "Answer: " + answer);
				out.println(answer.toString());
			}
		} catch (SocketTimeoutException e) {
			LOG.log(Level.WARNING, e.getMessage(), e);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);			
		} finally {
			setup.currentThreads--;
			try {
				socket.close();
			} catch (IOException e) {
				LOG.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

	private String getList() throws MBeanException, AttributeNotFoundException, InstanceNotFoundException, ReflectionException, IntrospectionException {
		StringBuilder answer = new StringBuilder();

		for (Configuration configuration : setup.getJMole().getConfiguration()) {
			MBeanFinder finder = configuration.getMBeanFinder();
			MBeanCollector collector = configuration.getMBeanCollector();
			for (ObjectName objectName : finder.getMatchingObjectNames()) {
				answer.append(fixForMunin(collector.getConstructedName(objectName)) + " ");
			}
		}

		return answer.toString();
	}

	private String executeMultigraphConfig(boolean dirtyconfig, boolean multigraph) throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IntrospectionException {
		return executeConfig(null, dirtyconfig, multigraph);
	}

	private String executeConfig(String part, boolean dirtyconfig, boolean multigraph) throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IntrospectionException {
		StringBuilder answer = new StringBuilder();
		 
		for (Configuration configuration : setup.getJMole().getConfiguration()) {
			MBeanFinder finder = configuration.getMBeanFinder();
			if (finder.getMatchingObjectNames().isEmpty()) {
				continue;
			}
			MBeanCollector collector = configuration.getMBeanCollector();
			PresentationInformation presentationInformation = configuration.getPresentationInformation();
					
			for (ObjectName objectName : finder.getMatchingObjectNames()) {
				if (multigraph) {
					answer.append("multigraph " + fixForMunin(collector.getConstructedName(objectName)) + "\n");
				} else {
					if (!part.equals(fixForMunin(collector.getConstructedName(objectName)).toLowerCase(Locale.ENGLISH))) {
						continue;
					}
				}
				if (presentationInformation.getCategory() != null) {
					answer.append("graph_category " + presentationInformation.getCategory() + "\n");
				}
				answer.append("graph_title " + collector.getConstructedName(objectName) + "\n");
				if (presentationInformation.getUnit() != null) {
					answer.append("graph_vlabel " + presentationInformation.getUnit() + "\n");
				}
				if (presentationInformation.getDescription() != null) {
					answer.append("graph_info " + presentationInformation.getDescription() + "\n");
				}
				// Hardcoded layout
				answer.append("graph_printf %.0lf\n");			
				
				for (String attribute : collector.getAttributes()) {
					String fixedAttribute = fixForMunin(attribute);
					answer.append(fixedAttribute + ".label " + presentationInformation.translateAttributeLabel(attribute) + "\n");
					
					String description = presentationInformation.getAttributeDescription(attribute);
					if (description != null) {
						answer.append(fixedAttribute + ".info " + description + "\n");
					}
					
					Threshold threshold = configuration.getThresholds().get(attribute);					
					if (threshold != null) {

						ThresholdValues individualValues = threshold.getIndividualThresholds().get(collector.getConstructedName(objectName));
						String low;
						String high;
						
						// Warnings
						if (individualValues == null) {
							low = threshold.getWarningLowThreshold();	
							high = threshold.getWarningHighThreshold();
						} else {
							low = individualValues.getWarningLowThreshold();
							high = individualValues.getWarningHighThreshold();
						}
						// TODO When Munin Ticket 1016 is impl. the calculation may not be needed					
						String tmp = Threshold.calculateThreshold(low, collector, objectName, attribute)
								+ ":" 
								+ Threshold.calculateThreshold(high, collector, objectName, attribute);						
						if (!tmp.equals(":")) {							
							answer.append(fixedAttribute + ".warning " + tmp + "\n");
						}

						// Critical
						if (individualValues == null) {
							low = threshold.getCriticalLowThreshold();	
							high = threshold.getCriticalHighThreshold();
						} else {
							low = individualValues.getCriticalLowThreshold();
							high = individualValues.getCriticalHighThreshold();
						}
						// TODO When Munin Ticket 1016 is impl. the calculation may not be needed					
						tmp = Threshold.calculateThreshold(low, collector, objectName, attribute) 
								+ ":" 
								+ Threshold.calculateThreshold(high, collector, objectName, attribute);
						if (!tmp.equals(":")) {
							answer.append(fixedAttribute + ".critical " + tmp + "\n");
						}
					}
				}

				if (dirtyconfig) {
					for (Entry<String, Object> entry : collector.getValues(objectName).entrySet()) {
						String valueString = translateValue(entry.getValue());
						answer.append(fixForMunin(entry.getKey()) + ".value " + valueString + "\n");
					}
				}
			}
		}
		return answer.append(".").toString();
	}
	
	private String executeFetch(String part) throws InstanceNotFoundException, ReflectionException, AttributeNotFoundException, MBeanException, IntrospectionException {
		StringBuilder answer = new StringBuilder();

                for (Configuration configuration : setup.getJMole().getConfiguration()) {
                        MBeanFinder finder = configuration.getMBeanFinder();
                        MBeanCollector collector = configuration.getMBeanCollector();

                        for (ObjectName objectName : finder.getMatchingObjectNames()) {
				if (part.equals(fixForMunin(collector.getConstructedName(objectName)).toLowerCase(Locale.ENGLISH))) {
                                	for (Entry<String, Object> entry : collector.getValues(objectName).entrySet()) {
                                       		String valueString = translateValue(entry.getValue());
                                       		answer.append(fixForMunin(entry.getKey()) + ".value " + valueString + "\n");
                                	}
				}
                        }
                }

                return answer.append(".").toString();
	}

	private String executeMultigraphFetch() throws InstanceNotFoundException, ReflectionException, AttributeNotFoundException, MBeanException, IntrospectionException {
		StringBuilder answer = new StringBuilder();
		 
		for (Configuration configuration : setup.getJMole().getConfiguration()) {
			MBeanFinder finder = configuration.getMBeanFinder();
			MBeanCollector collector = configuration.getMBeanCollector();
						
			for (ObjectName objectName : finder.getMatchingObjectNames()) {
				answer.append("multigraph " + fixForMunin(collector.getConstructedName(objectName)) + "\n");
				for (Entry<String, Object> entry : collector.getValues(objectName).entrySet()) {
					String valueString = translateValue(entry.getValue());
					answer.append(fixForMunin(entry.getKey()) + ".value " + valueString + "\n");
				}
			}			
		}
		return answer.append(".").toString();
	}

	// Munin can only handle numeric values
	private String translateValue(Object value) {
		if (value == null) {
			// TODO Or should we return 0?
			return "";
		}
		if (value instanceof Boolean) {
			if ((Boolean) value) {
				return "1";
			}
			return "0";
		}
		return value.toString();
	}
	
	// Munin can't handle special characters
	private String fixForMunin(String input) {
		return input.replaceAll("[^a-zA-Z0-9]", "_");
	}
	
}
