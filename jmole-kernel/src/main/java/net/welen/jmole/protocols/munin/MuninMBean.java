package net.welen.jmole.protocols.munin;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import net.welen.jmole.protocols.ProtocolMBean;

public interface MuninMBean extends ProtocolMBean {
	
	String getName();

	void setName(String name);

	String getNodeName();

        void setNodeName(String name);

	String getAddress();

	void setAddress(String address);

	Integer getPort();

	void setPort(Integer port);

	Integer getTcpReadTimeOut();

	void setTcpReadTimeOut(Integer timeout);

	Integer getMaxThreads();

	void setMaxThreads(Integer maxThreads);

	Integer getCurrentThreads();
	
}
