package net.welen.jmole.protocols.nrpe;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.protocols.AbstractProtocol;

public class NRPE extends AbstractProtocol implements NRPEMBean, Runnable {

	private final static Logger LOG = Logger.getLogger(NRPE.class.getName());

        private static String PROPERTY_NRPE_ADDRESS = "jmole.protocol.nrpe.address";
        private static String PROPERTY_NRPE_PORT = "jmole.protocol.nrpe.port";
        private static String PROPERTY_NRPE_ACCEPTED_HOSTS = "jmole.protocol.nrpe.acceptedHosts";
	private static String PROPERTY_NRPE_TCP_READ_TIMOUT = "jmole.protocol.nrpe.tcpReadTimeOut";
        private static String PROPERTY_NRPE_MAX_MESSAGE_SIZE = "jmole.protocol.nrpe.maxMessageSize";
        private static String PROPERTY_NRPE_USE_SSL = "jmole.protocol.nrpe.useSSL";
	
	private ServerSocket serverSocket;
	private String address;
	private Integer port;
	private String acceptedHosts[] = {"127.0.0.1"};
	private Integer tcpReadTimeOut;
	private boolean stopped = false;
	private boolean socketStopped = true;
	private Integer maxMessageSize = 1024;
		
	@Override
	public void startProtocol(JMole jmole) throws Exception {

		address = System.getProperty(PROPERTY_NRPE_ADDRESS);
		if (address == null) {
			address = "localhost";
		}

		port = Integer.getInteger(PROPERTY_NRPE_PORT);
		if (port == null) {
			port = 5666;
		}

                String acceptedHostsString = System.getProperty(PROPERTY_NRPE_ACCEPTED_HOSTS);
                if (acceptedHostsString != null) {
                        acceptedHosts = acceptedHostsString.split(",");
                }

		tcpReadTimeOut = Integer.getInteger(PROPERTY_NRPE_TCP_READ_TIMOUT);
		if (tcpReadTimeOut == null) {
			tcpReadTimeOut = 10000;
		}

		maxMessageSize = Integer.getInteger(PROPERTY_NRPE_MAX_MESSAGE_SIZE);
		if (maxMessageSize == null) {
			maxMessageSize = 1024;
		}

		LOG.log(Level.INFO, "JMole NRPE protocol starting; " + address + ":" + port);
		serverSocket = new ServerSocket(port, -1, InetAddress.getByName(address));
	//	serverSocket =  SSLServerSocketFactory.getDefault().createServerSocket(port, -1, InetAddress.getByName(address));
		serverSocket.setSoTimeout(1000);
		
		new Thread(this).start();

		super.startProtocol(jmole);
	}
	

	@Override
	public void stopProtocol() throws Exception {
		stopped = true;
		while (!socketStopped) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				LOG.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		super.stopProtocol();
		LOG.log(Level.INFO, "JMole NRPE protocol stopped");
	}
	
	public void run() {
		stopped = false;
		socketStopped = false;
		try {
			while (!stopped) {
				try {
					Socket socket = serverSocket.accept();					
					new NRPESocketHandler(socket, this).start();
				} catch (SocketTimeoutException e) {
					LOG.log(Level.FINEST, "SocketTimeoutException", e);
				} catch (IOException e) {
					LOG.log(Level.SEVERE, "Munin socket IOException", e);
				}
			}
		} finally {
			if (serverSocket != null && !serverSocket.isClosed()) {
				try {
					serverSocket.close();
				} catch (IOException e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}
			}
			socketStopped = true;
		}
	}

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public Integer getPort() {
		return port;
	}

	@Override
	public void setPort(Integer port) {
		this.port = port;
	}

	@Override
	public Integer getTcpReadTimeOut() {
		return tcpReadTimeOut;
	}

	@Override
	public void setTcpReadTimeOut(Integer timeout) {
		this.tcpReadTimeOut = timeout;
	}

        @Override
        public Integer getMaxMessageSize() {
                return maxMessageSize;
        }

        @Override
        public void handleWarning(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		// Ignored in Munin protocol
        }

        @Override
        public void handleCritical(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		// Ignored in Munin protocol
        }

}
