package net.welen.jmole.protocols.logstash;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.Date;

import javax.net.ssl.SSLSocketFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.protocols.AbstractIntervalProtocol;
import net.welen.jmole.protocols.ProtocolMessagePlaceholder;

public class Logstash extends AbstractIntervalProtocol implements LogstashMBean {

	private final static Logger LOG = Logger.getLogger(Logstash.class.getName());

	private static String PROPERTY_LOGSTASH_USESSL = "jmole.protocol.logstash.useSSL";
	private static String PROPERTY_LOGSTASH_HOST = "jmole.protocol.logstash.host";
	private static String PROPERTY_LOGSTASH_PORT = "jmole.protocol.logstash.port";
	private static String PROPERTY_LOGSTASH_KEEPALIVE = "jmole.protocol.logstash.keepAlive";
	private static String PROPERTY_LOGSTASH_INTERVAL = "jmole.protocol.logstash.interval";
	
	private Boolean useSSL;
	private String host;
	private Integer port;
	private Boolean keepAlive;

        private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();

	private Socket socket;
		
	@Override
	public void startProtocol(JMole jmole) throws Exception {
		useSSL = Boolean.getBoolean(PROPERTY_LOGSTASH_USESSL);
		if (useSSL == null) {
			useSSL = false;
		}

		host = System.getProperty(PROPERTY_LOGSTASH_HOST);
		if (host == null) {
			host = "localhost";
		}
		
		port = Integer.getInteger(PROPERTY_LOGSTASH_PORT);
		if (port == null) {
			port = 5000;
		}
				
		Long interval = Long.getLong(PROPERTY_LOGSTASH_INTERVAL);
		if (interval != null) {
			setInterval(interval);
		}

		keepAlive = Boolean.getBoolean(PROPERTY_LOGSTASH_KEEPALIVE);
		if (keepAlive == null) {
			keepAlive = false;
		}

		super.startProtocol(jmole);
		LOG.log(Level.INFO, "JMole Logstash protocol started: " + host + ":" + port + " interval=" + getInterval());
	}
	

	@Override
	public void stopProtocol() throws Exception {
		LOG.log(Level.INFO, "Stopping JMole Logstash protocol");
		super.stopProtocol();
		LOG.log(Level.INFO, "JMole Logstash protocol stopped");
	}

	@Override
	public boolean useSSL() {
		return useSSL;
	}

	@Override
	public String getHost() {
		return host;
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public boolean getKeepAlive() {	
		return keepAlive;
	}

	@Override
	protected void handleMeasurement(String category, String name, String attribute, Object value,
			PresentationInformation presentationInformation) throws Exception {		
		sendToLogstash(gson.toJson(new ProtocolMessagePlaceholder("metric", new Date(), null, category, name, attribute, value, presentationInformation)));
	}

	private void sendToLogstash(String data) {					
		LOG.log(Level.FINE, "Sending data to logstash: " + data);
		PrintWriter out = null;
		try {
			out = new PrintWriter(getSocketOutputStream());
			out.write(data + "\n");
			out.flush();
		} catch (IOException e) {			
			socket = null;		
			LOG.log(Level.SEVERE, "Couldn't send data to logstash: " + data, e);
		} finally {
			if (!keepAlive) {
				socket = null;		
			}
		}	
	}
	
	private OutputStream getSocketOutputStream() throws UnknownHostException, IOException {
		if (socket == null) {
			if (useSSL) {				
				socket = SSLSocketFactory.getDefault().createSocket(host, port);
			} else {
				socket = new Socket(host, port);
			}
		}
		return socket.getOutputStream();
	}

        @Override
        public void handleWarning(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		sendToLogstash(gson.toJson(new ProtocolMessagePlaceholder("warning", new Date(),  message, presentationInformation.getCategory(), name, attribute, null, presentationInformation)));
        }

        @Override
        public void handleCritical(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		sendToLogstash(gson.toJson(new ProtocolMessagePlaceholder("critical", new Date(), message, presentationInformation.getCategory(), name, attribute, null, presentationInformation)));
        }

}
