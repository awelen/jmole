package net.welen.jmole.protocols;

import java.util.logging.Level;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.Notification;

import net.welen.jmole.Utils;
import net.welen.jmole.JMole;
import net.welen.jmole.threshold.JMoleNotification;
import net.welen.jmole.threshold.NotificationUserData;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

abstract public class AbstractProtocol implements Protocol, ProtocolMBean {
	private final static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(AbstractProtocol.class.getName());

	
	private String name;
        private JMole jmole;
		
        private MBeanServer server = Utils.getMBeanServer();

        @Override
        public void startProtocol(JMole jmole) throws Exception {
                this.jmole = jmole;
		server.addNotificationListener(new ObjectName(JMole.OBJECT_NAME), this, null, null);
	}

        @Override
        public void stopProtocol() throws Exception {
		server.removeNotificationListener(new ObjectName(JMole.OBJECT_NAME), this);
	}

        @Override
        public JMole getJMole() {
		return jmole;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override	
	public String getName() {
		return name;
	}

        @Override
        public void handleNotification(Notification notification, Object handback) {

		if (notification instanceof JMoleNotification) {
			LOG.log(Level.FINE, "Notification recieved");

			JMoleNotification aJMoleNotification = (JMoleNotification) notification;
			NotificationUserData notificationUserData = (NotificationUserData) aJMoleNotification.getUserData();
			String name = notificationUserData.getName();
			String attribute = notificationUserData.getAttribute();
			String type = aJMoleNotification.getType();

			try {
				if (type.equals(JMoleNotification.TYPE_WARNING)) {
					handleWarning(name, attribute, notificationUserData.getPresentationInformation(), aJMoleNotification.getMessage());
				} else if (type.equals(JMoleNotification.TYPE_CRITICAL)) {
					handleCritical(name, attribute, notificationUserData.getPresentationInformation(), aJMoleNotification.getMessage());
				} else {
					LOG.log(Level.SEVERE, "Unknown type in notification: " + type);
				}
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Failed in handlin notification", e);
			}
		} else {
			LOG.log(Level.SEVERE, "Unknown notification recieved: " + notification);
		}
        }

}
