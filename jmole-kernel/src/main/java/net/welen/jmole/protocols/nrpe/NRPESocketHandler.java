package net.welen.jmole.protocols.nrpe;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.SocketException;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;

import net.welen.jmole.Configuration;
import net.welen.jmole.collector.MBeanCollector;
import net.welen.jmole.finder.MBeanFinder;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.threshold.Threshold;
import net.welen.jmole.threshold.ThresholdValues;

public class NRPESocketHandler extends Thread {
	
	private final static Logger LOG = Logger.getLogger(NRPESocketHandler.class.getName());

	private Socket socket;
	private NRPE setup;

	public NRPESocketHandler(Socket socket, NRPE setup) {
		this.socket = socket;
		this.setup = setup;
		this.setName("JMole NRPE protocol thread");
	}

	public void run() {		
			
		DataInputStream in = null;
		DataOutputStream out = null;
		try {
			socket.setSoTimeout(setup.getTcpReadTimeOut());

			in = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());

			NRPEMessage request = new NRPEMessage(in);
			LOG.log(Level.FINE, "Incoming message: " + request.toString());

			NRPEMessage answer = null;
			String commands[] = request.getBufferAsString().split("!");
			String command = null;
			if (commands.length == 0) {
				answer = new NRPEMessage(request.getVersion(), 2, 3, "Missing command in buffer".getBytes());
			} else {
				command = commands[0];

				Integer maxMessageSize = setup.getMaxMessageSize();
				if (command.equals("checkWarnings")) {
					String problems = setup.getJMole().warningMessages().toString();
					if (problems.length() > maxMessageSize) {
						problems = problems.substring(0, maxMessageSize);
					}
					if (!problems.equals("{}")) {
						answer = new NRPEMessage(request.getVersion(), 2, 1, problems.getBytes());
					} else {
						answer = new NRPEMessage(request.getVersion(), 2, 0, "No warnings detected".getBytes());
					}
				} else if (command.equals("checkCriticals")) {
					String problems = setup.getJMole().criticalMessages().toString();
					if (problems.length() > maxMessageSize) {
                                                problems = problems.substring(0, maxMessageSize);
                                        }
					if (!problems.equals("{}")) {
                                                answer = new NRPEMessage(request.getVersion(), 2, 2, problems.getBytes());
                                        } else {
						answer = new NRPEMessage(request.getVersion(), 2, 0, "No critical errors detected".getBytes());
                                        }
				} else {
					LOG.log(Level.SEVERE, "Unknown command: \"" + command + "\" in buffer in message: " + request.toString());
					answer = new NRPEMessage(request.getVersion(), 2, 3, "Unknown command in buffer".getBytes());
				}
			}
			LOG.log(Level.FINE, "Outgoing message: " + answer.toString());

			answer.sendMessage(out);
			out.flush();

		} catch (SocketTimeoutException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} catch (SocketException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} catch (IOException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} catch (AttributeNotFoundException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} catch (InstanceNotFoundException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} catch (MBeanException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} catch (ReflectionException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (out!= null) {
					out.close();
				}
				socket.close();
			} catch (IOException e) {
				LOG.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

}
