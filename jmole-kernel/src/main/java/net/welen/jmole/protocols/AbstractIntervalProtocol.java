package net.welen.jmole.protocols;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

abstract public class AbstractIntervalProtocol extends AbstractProtocol implements IntervalProtocolMBean, Runnable {

	private final static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(AbstractIntervalProtocol.class.getName());
	private static final char SEP = '/';	
	static final long DEFAULT_INTERVAL = 60000l;

	private Thread collector;
	private boolean stopRequested = false;
	private boolean threadStopped = false;
	private long interval = DEFAULT_INTERVAL;

	@Override
	public void startProtocol(JMole jmole) throws Exception {
		super.startProtocol(jmole);

		collector = new Thread(this);
		collector.setName("JMole protocol thread: " + getName());
		collector.start();
		LOG.log(Level.FINE, "JMole protocol thread started: Interval=" + getInterval());
	}

	@Override
	public void stopProtocol() throws Exception {
		LOG.log(Level.FINE, "Stopping protocol thread");
		stopRequested = true;
		collector.interrupt();
		while (!threadStopped) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				LOG.log(Level.FINE, e.getMessage(), e);
			}
		}
		super.stopProtocol();
		LOG.log(Level.FINE, "Protocol thread stopped");
	}

	@Override
	public void run() {
		stopRequested = false;
		threadStopped = false;
		try {
			while (!stopRequested) {
				try {
					Thread.sleep(getInterval());
				} catch (InterruptedException e) {
					LOG.log(Level.FINE, e.getMessage(), e);
				}

				if (stopRequested) {
					return;
				}

				try {
					handleMeasurements();
				} catch (Exception e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		} finally {
			threadStopped = true;
		}
	}

	private void handleMeasurements()
			throws InstanceNotFoundException, AttributeNotFoundException, ReflectionException, MBeanException, IntrospectionException {
		Map<Object, PresentationInformation> presentationInformationMap = new HashMap<Object, PresentationInformation>();
		for (Entry<String, List<Map<String, Map<String, Object>>>> categoryEntry : getJMole().collectMeasurements(presentationInformationMap).entrySet()) {
			Iterator<Map<String, Map<String, Object>>> iter = categoryEntry.getValue().iterator();
			while (iter.hasNext()) {
				for (Entry<String, Map<String, Object>> nameEntry : iter.next().entrySet()) {
					for (Entry<String, Object> attributeEntry : nameEntry.getValue().entrySet()) {
						String piKey = categoryEntry.getKey() + nameEntry.getKey();
						PresentationInformation presentationInformation = presentationInformationMap.get(piKey);
						if (presentationInformation == null) {
							LOG.severe("No presentation information found for: " + piKey + ", Skipping it");
							continue;
						}
						try {
							if (attributeEntry.getValue() != null) {
								handleMeasurement(categoryEntry.getKey(), nameEntry.getKey(), attributeEntry.getKey(), attributeEntry.getValue(), presentationInformation);
							}
						} catch (Exception e) {
							LOG.log(Level.SEVERE, e.getMessage() + ": " + categoryEntry.getKey() + SEP
									+ nameEntry.getKey() + SEP + attributeEntry.getKey(), e);
						}
					}
				}
			}
		}
	}

	public long getInterval() {
		return interval;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	abstract protected void handleMeasurement(String category, String name, String attribute, Object value, PresentationInformation presentationInformation) throws Exception;	

}
