package net.welen.jmole.protocols.nrpe;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Based on info from: https://github.com/stockholmuniversity/Nagios-NRPE/blob/master/share/protocol-nrpe.md
 */


public class NRPEMessage {

        private final static Logger LOG = Logger.getLogger(NRPEMessage.class.getName());

	private int version = 0;
	private int type = 0;
	private int crc32 = 0;
	private int resultCode = 0;
	private int alignment = 0;
	private byte[] buffer;
	private int padding = 0;
	
	public NRPEMessage(DataInputStream in) throws IOException {
		version = in.readShort();							// Version 2 bytes
		type = in.readShort();								// Type 2 bytes
		crc32 = in.readInt();								// CRC32 4 bytes
		resultCode = in.readShort();							// ResultCode 2 bytes

		if (version < 2 || version > 4) {
			throw new IOException("NRPE Version " + version + " not supported");
		}

		int bufferLength = 1024;							// BufferLength v2
		if (version > 2) {
			alignment = in.readShort();						// Alignment
			bufferLength = in.readInt();						// BufferLength v3/v4
		}
		buffer = new byte[bufferLength];
		in.read(buffer);

		if (version == 2) {
 			padding = in.readShort();							// Paddingn 2 bytes
		}

		int calcValue = calcCRC32();
		if (crc32 != calcValue) {
			throw new IOException("CRC32 Error: " + crc32 + "!=" + calcValue);
		}
	}

	public NRPEMessage(int version, int type, int resultCode, byte[] buffer) {
		this.version = version;
		this.type = type;
		this.resultCode = resultCode;
		this.buffer = buffer;
	}

	public int getVersion() {
		return version;
	}

	public int getType() {
                return type;
        }

	public long getCRC32() {
                return crc32;
        }

	public int getResultCode() {
                return resultCode;
        }

	public void sendMessage(DataOutputStream out) throws IOException {
		byte[] data = toBytes(calcCRC32());
		out.write(data);
	}

	private byte[] toBytes(int crc32Value) throws IOException {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bytes);

		out.writeShort(version);				// Version 2 bytes
                out.writeShort(type);					// Type 2 bytes
                out.writeInt(crc32Value);				// CRC32 4 bytes
                out.writeShort(resultCode);				// ResultCode 2 bytes
                if (version > 2) {
                        out.writeShort(alignment);			// Alignement 2 bytes
                        out.writeInt(buffer.length);			// BufferLength 4 bytes
                }
                out.write(buffer);
                if (version == 2) {
                        int length = 1024 - buffer.length;
                        if (length > 0) {
                                out.write(new byte[length]);
                        }
			out.writeShort(padding);
                }
		out.close();

		return bytes.toByteArray(); 
	}

	private int calcCRC32() throws IOException {
		CRC32 crcAlg = new CRC32();
		crcAlg.update(toBytes(0));

		return (int) crcAlg.getValue();
	}

	public String getBufferAsString() {
		return new String(buffer).trim();
	}

	public String toString() {
		return "Version: " + version + ", type: " + type + ", crc32: " + crc32 + ", resultCode: " + resultCode + ", buffer: " + getBufferAsString() + ", alignment: " + alignment + ", padding: " + padding;
 	}  	

}
