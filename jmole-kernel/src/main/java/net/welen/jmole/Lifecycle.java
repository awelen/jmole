package net.welen.jmole;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import net.welen.jmole.protocols.Protocol;
import net.welen.jmole.protocols.logstash.Logstash;
import net.welen.jmole.protocols.munin.Munin;
import net.welen.jmole.protocols.nrpe.NRPE;

/**
 * POJO to handle the lifecycle of JMole.
 */
public class Lifecycle {

	private final static Logger LOG = Logger.getLogger(Lifecycle.class.getName());

	private static final String PROTOCOLS_PROPERTY_PREFIX = "jmole.protocol.";
	private static final String PROTOCOLS_PROPERTY_ENABLED_SUFFIX = ".enabled";
	private static final String PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX = ".classname";
	private static final String PROTOCOLS_PROPERTY_JAR_SUFFIX = ".jars";
	private static final String PROTOCOL_MBEAN_NAME_PREFIX="net.welen.jmole.protocol:name=";

	private static JMole jmole;
	private static boolean running = false;
	
	private static MBeanServer server = Utils.getMBeanServer();

	private static List<Protocol> protocols = new ArrayList<Protocol>();

	/**
	 * Starts up JMole and all activated protocols
	 */
	public static synchronized void setup() {
		if (running) {
			return;
		}
		running = true;

		LOG.log(Level.INFO, "Starting JMole");
		try {
			jmole = new JMole();
			jmole.register();
			jmole.configure();

			// Add setup for built-in protocols
			if (Boolean.getBoolean(PROTOCOLS_PROPERTY_PREFIX + "logger" + PROTOCOLS_PROPERTY_ENABLED_SUFFIX)) {
				System.setProperty(PROTOCOLS_PROPERTY_PREFIX + "logger" + PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX,
						net.welen.jmole.protocols.logger.Logger.class.getCanonicalName());
			}
			if (Boolean.getBoolean(PROTOCOLS_PROPERTY_PREFIX + "munin" + PROTOCOLS_PROPERTY_ENABLED_SUFFIX)) {
				System.setProperty(PROTOCOLS_PROPERTY_PREFIX + "munin" + PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX,
						Munin.class.getCanonicalName());
			}
			if (Boolean.getBoolean(PROTOCOLS_PROPERTY_PREFIX + "logstash" + PROTOCOLS_PROPERTY_ENABLED_SUFFIX)) {
				System.setProperty(PROTOCOLS_PROPERTY_PREFIX + "logstash" + PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX,
						Logstash.class.getCanonicalName());
			}
			if (Boolean.getBoolean(PROTOCOLS_PROPERTY_PREFIX + "nrpe" + PROTOCOLS_PROPERTY_ENABLED_SUFFIX)) {
				System.setProperty(PROTOCOLS_PROPERTY_PREFIX + "nrpe" + PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX,
						NRPE.class.getCanonicalName());
			}
			if (Boolean.getBoolean(PROTOCOLS_PROPERTY_PREFIX + "zabbix" + PROTOCOLS_PROPERTY_ENABLED_SUFFIX)) {
				System.setProperty(PROTOCOLS_PROPERTY_PREFIX + "zabbix" + PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX,
						"net.welen.jmole.protocols.zabbix.Zabbix");
			}
			if (Boolean.getBoolean(PROTOCOLS_PROPERTY_PREFIX + "statsd" + PROTOCOLS_PROPERTY_ENABLED_SUFFIX)) {
				System.setProperty(PROTOCOLS_PROPERTY_PREFIX + "statsd" + PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX,
						"net.welen.jmole.protocols.statsd.Statsd");
			}
			if (Boolean.getBoolean(PROTOCOLS_PROPERTY_PREFIX + "syslog" + PROTOCOLS_PROPERTY_ENABLED_SUFFIX)) {
				System.setProperty(PROTOCOLS_PROPERTY_PREFIX + "syslog" + PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX,
						"net.welen.jmole.protocols.syslog.Syslog");
			}
			if (Boolean.getBoolean(PROTOCOLS_PROPERTY_PREFIX + "cloudwatch" + PROTOCOLS_PROPERTY_ENABLED_SUFFIX)) {
				System.setProperty(PROTOCOLS_PROPERTY_PREFIX + "cloudwatch" + PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX,
						"net.welen.jmole.protocols.cloudwatch.CloudWatch");
			}

			// Load protocols
			loadProtocols();

			// Start protocols
			for (Protocol protocol : protocols) {
				try {					
					protocol.startProtocol(jmole);
					server.registerMBean(protocol, getProtocolObjectName(protocol));
				} catch (Throwable t) {
					LOG.log(Level.SEVERE, "Problem starting protocol: " + protocol.getName(), t);
				}
			}
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

	/**
	 * Stops JMole and all activated protocols
	 */
	public static void cleanup() {
		LOG.log(Level.INFO, "Stopping JMole");
		try {
			// Stop protocols
			for (Protocol protocol : protocols) {
				try {
					protocol.stopProtocol();
					server.unregisterMBean(getProtocolObjectName(protocol));
				} catch (Throwable t) {
					LOG.log(Level.SEVERE, "Problem stopping protocol: " + protocol.getName(), t);
				}
			}
			jmole.unregister();
			running = false;
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

	private static ObjectName getProtocolObjectName(Protocol protocol) throws MalformedObjectNameException {
		return  new ObjectName(PROTOCOL_MBEAN_NAME_PREFIX + protocol.getName());
	}

	public static JMole getJMoleInstance() {
		return jmole;
	}

	private static void loadProtocols()
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		protocols = new ArrayList<Protocol>();

		// Search for all enabled protocols (All system properties matching
		// jmole.protocol.*.enabled=true)
		for (Object prop : System.getProperties().keySet()) {
			if (prop instanceof String) {
				String propString = (String) prop;
				if (propString.startsWith(PROTOCOLS_PROPERTY_PREFIX)
						&& propString.endsWith(PROTOCOLS_PROPERTY_ENABLED_SUFFIX) && Boolean.getBoolean(propString)) {

					String protocolName = propString.replaceFirst("^" + PROTOCOLS_PROPERTY_PREFIX, "")
							.replaceFirst(PROTOCOLS_PROPERTY_ENABLED_SUFFIX + "$", "");
					LOG.log(Level.INFO, "Loading protocol: " + protocolName);

					// Create protocol using the specified class
					String systemPropertyClassname = PROTOCOLS_PROPERTY_PREFIX + protocolName
							+ PROTOCOLS_PROPERTY_CLASSNAME_SUFFIX;
					String systemPropertyJARfiles = PROTOCOLS_PROPERTY_PREFIX + protocolName
							+ PROTOCOLS_PROPERTY_JAR_SUFFIX;

					String className = System.getProperty(systemPropertyClassname);
					if (className != null) {
						Class<?> clazz;

						// Load class (and JAR if needed)
						String jarNames = System.getProperty(systemPropertyJARfiles);					
						if (jarNames == null) {
							LOG.log(Level.INFO, "No protocol jars to load");
							clazz = Class.forName(className);
						} else {
							LOG.log(Level.INFO, "Loading protocol jars: " + jarNames);
							List<URL> urls = new ArrayList<URL>();
							for (String jar : jarNames.split(",")) {
								urls.add(new URL(jar));
							}
							URL[] urlArray = new URL[urls.size()];
							urlArray = urls.toArray(urlArray);

							clazz = Class.forName(className, true,
									new URLClassLoader(urlArray, Lifecycle.class.getClassLoader()));
						}

						Constructor<?> ctor = clazz.getConstructor();
						Protocol p = (Protocol) ctor.newInstance();
						p.setName(protocolName);
						protocols.add(p);
					} else {
						LOG.log(Level.SEVERE, "Protocol enabled but no classname found using system property: "
								+ systemPropertyClassname);
					}
				}
			}
		}
	}

}
