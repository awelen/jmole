package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Properties;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.NotificationBroadcasterSupport;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;
import javax.xml.XMLConstants;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.threshold.Threshold;
import net.welen.jmole.threshold.ThresholdThread;

import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.beans.XMLDecoder;
import java.beans.ExceptionListener;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * The MBean implementation of JMole and more or less the main class
 */
public class JMole extends NotificationBroadcasterSupport implements JMoleMBean {

	private final static Logger LOG = Logger.getLogger(JMole.class.getName());

	static public final String OBJECT_NAME = "net.welen.jmole:service=jmole";
	static public final String CONFIG_FILENAMES_PROPERTY = "jmole.config.filenames";
	static public final String CONFIG_FILENAME_PROPERTY_PREFIX = "jmole.config.filename.";
	static public final String CONFIG_FILENAME_XSLT_PROPERTY_PREFIX = "jmole.config.xslt.";
	static public final String CONFIG_LEVEL_PROPERTY = "jmole.config.level";

	public static final String NOTIFICATION_TYPE = "jmole.reconfigured";

	private MBeanServer server = Utils.getMBeanServer();
	private List<Configuration> configurations = new ArrayList<Configuration>();

	private Map<Long, ThresholdThread> thresholdThreads = new HashMap<Long, ThresholdThread>();		

        private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
	
	// Inner class that is used by XML libs
	private static class SimpleExceptionListener implements ExceptionListener {
		private Exception e = null;

		public void exceptionThrown(Exception e) {
			this.e = e;
		}

		Exception getException() {
			return e;
		}
	}
	
	/**
	 * Register the JMole MBean
	 * 
	 * @throws MalformedObjectNameException
	 * @throws InstanceAlreadyExistsException
	 * @throws MBeanRegistrationException
	 * @throws NotCompliantMBeanException
	 * @throws InstanceNotFoundException
	 */
	public void register() throws InstanceAlreadyExistsException, MBeanRegistrationException,
			NotCompliantMBeanException, MalformedObjectNameException, InstanceNotFoundException {
		LOG.log(Level.FINE, "Registering JMole MBean");
		server.registerMBean(this, new ObjectName(OBJECT_NAME));
	}

	/**
	 * Unregister the JMole MBean
	 * 
	 * @throws MalformedObjectNameException
	 * @throws InstanceNotFoundException
	 * @throws MBeanRegistrationException
	 */
	public void unregister() throws MalformedObjectNameException, InstanceNotFoundException, MBeanRegistrationException {
		LOG.log(Level.FINE, "Removing JMole MBean");
		deactivateThresholds();
		server.unregisterMBean(new ObjectName(OBJECT_NAME));
	}

	@Override
	public synchronized void configure() throws MalformedObjectNameException, FileNotFoundException, MBeanException,
			AttributeNotFoundException, InstanceNotFoundException, ReflectionException, IntrospectionException {
		List<Configuration> newConfigurations = Collections.synchronizedList(new ArrayList<Configuration>());

		String fileNames = getConfigFileNames();

		for (String fileName : fileNames.split("\\|")) {
			LOG.log(Level.INFO, "Configuring JMole with config file: " + fileName);

			XMLDecoder decoder = null;
			InputStream configStream = null;
			SimpleExceptionListener myListener = new SimpleExceptionListener();
			byte[] data = null;
			try {
				// Get inputstream
				if (new File(fileName).exists()) {
					configStream = new BufferedInputStream(new FileInputStream(fileName));
				} else {
					configStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
				}

				if (configStream == null) {
					LOG.log(Level.SEVERE, String.format("Unable to load config from '%s'", fileName));
					continue;
				}

				// Put all in memory
				try {
					data = readData(configStream);
				} catch (IOException e) {
					LOG.log(Level.SEVERE, String.format("Unable to load config from '%s'", fileName), e);
					continue;
				}

				// Replace all ${systemProperty} syntax
				data = replaceAllSystemProperties(data);


				// Find matching XSLT (if any)
				String xslt = null;
				for (Entry<Object, Object> entry : System.getProperties().entrySet()) {				
					if (entry.getKey() instanceof String && entry.getValue() instanceof String) {
						String key = (String) entry.getKey();
						String value = (String) entry.getValue();
						if (key.startsWith(CONFIG_FILENAME_PROPERTY_PREFIX) && value.equals(fileName)) {
							String part = key.substring(CONFIG_FILENAME_PROPERTY_PREFIX.length());
							xslt = System.getProperty(CONFIG_FILENAME_XSLT_PROPERTY_PREFIX + part);
						}
					}
				}					
					
				if (xslt != null) {
					try {
						LOG.log(Level.INFO,
								String.format("Applying XSLT: '%s' on config file: '%s'", xslt, fileName));
						data = transformData(data, xslt);
					} catch (Exception e) {
						LOG.log(Level.SEVERE, String.format(
								"Unable to apply specified XSLT transformation to config file '%s'", fileName), e);
						continue;
					}
				}

				try {
					validateData(data);
				} catch (Exception e) {
					LOG.log(Level.SEVERE, String.format("Unable to validate config file '%s'", fileName), e);
					continue;
				}

				try {
					data = transformData(data, "META-INF/JMole.xsl");
				} catch (Exception e) {
					LOG.log(Level.SEVERE, String.format("Unable to transform config file '%s'", fileName), e);
					continue;
				}

				// Parse data, OSGI really messed it up so we needed to set the classloader to use for the XMLDecoder
				decoder = new XMLDecoder(new ByteArrayInputStream(data), null, myListener, getClass().getClassLoader());

				Object newDecodedConfigurations = null;
				try {
					newDecodedConfigurations = decoder.readObject();
				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.log(Level.SEVERE,
							String.format("Config file is corrupt '%s'%n== CONFIG START ==%n%s%n== CONFIG STOP ==",
									fileName, new String(data)), e);
					continue;
				}

				newConfigurations.addAll((List<Configuration>) newDecodedConfigurations);
			} finally {
				Exception exception = myListener.getException();
				if (exception != null) {
					LOG.log(Level.SEVERE,
							String.format("Config file is corrupt '%s'%n== CONFIG START ==%n%s%n== CONFIG STOP ==",
									fileName, new String(data)), exception);
				}

				if (decoder != null) {
					decoder.close();
				}
				if (configStream != null) {
					try {
						configStream.close();
					} catch (IOException e) {
						LOG.log(Level.WARNING, "Couldn't close configStream", e);
					}
				}
			}
		}

		// Remove all configurations that don't meet the level criteria
		int level = getLevel();
		if (level < 1 || level > 5) {
			throw new RuntimeException(CONFIG_LEVEL_PROPERTY + " must be 1-5");
		}
		List<Configuration> levelFilteredConfigurations = new ArrayList<Configuration>();
		for (Configuration configuration : newConfigurations) {
			if (configuration.getLevel() <= level) {
				levelFilteredConfigurations.add(configuration);
			}
		}

		configurations = levelFilteredConfigurations;

		deactivateThresholds();
		activateThresholds();
	}

	public String getConfigFileNames() {
		String fileNames = "JMole_JVM.xml";
		if (System.getProperty(CONFIG_FILENAMES_PROPERTY) != null) {
			LOG.log(Level.WARNING, "JMole config property " + CONFIG_FILENAMES_PROPERTY + " is deprecated. Use " + CONFIG_FILENAME_PROPERTY_PREFIX + "* instead");
			fileNames = System.getProperty(CONFIG_FILENAMES_PROPERTY);
		} else {
			StringBuilder configs = new StringBuilder();
			for (Object prop : System.getProperties().keySet()) {				
				if (prop instanceof String) {
					String propString = (String) prop;					
					if (propString.startsWith(CONFIG_FILENAME_PROPERTY_PREFIX)) {
						if (configs.length() > 0) {
							configs.append("|");
						}
						configs.append(System.getProperty(propString));
					}
				}
			}
			if (configs.length() > 0) {
				fileNames = configs.toString();
			} else {			
				LOG.log(Level.INFO, "No config file(s) configured. Using: JMole_JVM.xml");
			}
		}
		return fileNames;
	}

	private byte[] replaceAllSystemProperties(byte[] data) {
		String stringData = new String(data);

		Properties clone = new Properties();
		clone.putAll(System.getProperties());
		for (Entry<Object, Object> propEntry : clone.entrySet()) {
			String key = (String) propEntry.getKey();
			String value = (String) propEntry.getValue();
			stringData = stringData.replaceAll("\\$\\{" + key + ":.*\\}", value);
			stringData = stringData.replaceAll("\\$\\{" + key + "\\}", value);
		}

		stringData = stringData.replaceAll("\\$\\{.*:|\\}", "");

		return stringData.getBytes();
	}

	private byte[] readData(InputStream inputStream) throws IOException {
		ByteArrayOutputStream data = new ByteArrayOutputStream();

		try {
			byte[] buffer = new byte[1024];
			int read = 0;
			while ((read = inputStream.read(buffer)) != -1) {
				data.write(buffer, 0, read);
			}
		} finally {
			try {
				data.close();
			} catch (IOException e) {
				LOG.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		return data.toByteArray();
	}

	private void validateData(byte[] data) throws SAXException, IOException {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		try {
			schema = factory.newSchema(new StreamSource(
					Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/JMole.xsd")));
		} catch (SAXParseException e) {
			ClassLoader tccl = Thread.currentThread().getContextClassLoader();
			try {
				Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
				schema = factory.newSchema(new StreamSource(
						Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/JMole.xsd")));
			} finally {
				Thread.currentThread().setContextClassLoader(tccl);
			}
		}
		Validator validator = schema.newValidator();
		validator.validate(new StreamSource(new ByteArrayInputStream(data)));
	}

	private byte[] transformData(byte[] data, String xsltFile) throws TransformerException {
		ByteArrayOutputStream answer = new ByteArrayOutputStream();
		StreamResult outputStream = new StreamResult(answer);

		TransformerFactory factory = TransformerFactory.newInstance();
		factory.setErrorListener(new ErrorListener() {		
			@Override
			public void warning(TransformerException exception) throws TransformerException {
				LOG.log(Level.FINE, exception.getMessage(), exception);
			}			
			@Override
			public void fatalError(TransformerException exception) throws TransformerException {
				LOG.log(Level.FINE, exception.getMessage(), exception);
			}			
			@Override
			public void error(TransformerException exception) throws TransformerException {
				LOG.log(Level.FINE, exception.getMessage(), exception);
			}
		});		
		
		if (new File(xsltFile).exists()) {
			Source xslt;
			try {
				xslt = new StreamSource(new BufferedInputStream(new FileInputStream(xsltFile)));
				Transformer transformer = factory.newTransformer(xslt);
				transformer.transform(new StreamSource(new ByteArrayInputStream(data)), outputStream);
			} catch (FileNotFoundException e) {
				LOG.log(Level.SEVERE, "XSLT file: " + xsltFile + " not found.", e);
				throw new TransformerException(e.getMessage(), e);
			}
		} else {
			try {
				LOG.log(Level.FINE, "Getting the XSLT file from Thread.currentThread().getContextClassLoader()");
				Source xslt = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream(xsltFile));
				Transformer transformer = factory.newTransformer(xslt);
				transformer.transform(new StreamSource(new ByteArrayInputStream(data)), outputStream);
			} catch (Exception e) {
				ClassLoader tccl = Thread.currentThread().getContextClassLoader();
				try {
					LOG.log(Level.FINE, "Getting the XSLT file from getClass().getClassLoader()");
					Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
					Source xslt = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream(xsltFile));
					Transformer transformer = factory.newTransformer(xslt);
					transformer.transform(new StreamSource(new ByteArrayInputStream(data)), outputStream);
				} finally {
					Thread.currentThread().setContextClassLoader(tccl);
				}
			}
		}
		return answer.toByteArray();
	}

	/**
	 * Get the current configuration
	 * 
	 * @return The configuration in for av classes
	 */
	public List<Configuration> getConfiguration() {
		return configurations;
	}

	@Override
	public Map<String, List<Map<String, Map<String, Object>>>> collectMeasurements()
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {
		return collectMeasurements((Map<Object, PresentationInformation>) null);
	}

	public Map<String, List<Map<String, Map<String, Object>>>> collectMeasurements(Map<Object, PresentationInformation> presentationInformation)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {

		Map<String, List<Map<String, Map<String, Object>>>> answer = new HashMap<String, List<Map<String, Map<String, Object>>>>();
		for (Configuration configuration : configurations) {
			String category = configuration.getPresentationInformation().getCategory();

			if (answer.containsKey(category)) {
				continue;
			}

			Map<String, Map<String, Object>> data = collectMeasurements(category, presentationInformation);

			if (!data.isEmpty()) {
				if (answer.get(category) == null) {
					answer.put(category, new ArrayList<Map<String, Map<String, Object>>>());
				}
				answer.get(category).add(data);
			}

		}
		return answer;
	}

	@Override
	public String collectMeasurementsAsJSON()
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {
		return gson.toJson(collectMeasurements());
	}

	@Override
	public Map<String, Map<String, Object>> collectMeasurements(String category)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {		
		return collectMeasurements(category, (Map<Object, PresentationInformation>) null);
	}

	public Map<String, Map<String, Object>> collectMeasurements(String category, Map<Object, PresentationInformation> presentationInformation)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {

		Map<String, Map<String, Object>> answer = new HashMap<String, Map<String, Object>>();
		for (Configuration configuration : configurations) {
			if (configuration.getPresentationInformation().getCategory().equals(category)) {
				for (ObjectName objectName : configuration.getMBeanFinder().getMatchingObjectNames()) {
					if (presentationInformation != null) {
						LOG.log(Level.FINE, "PresentationInformation saved for: " + category + configuration.getMBeanCollector().getConstructedName(objectName));
						presentationInformation.put(category + configuration.getMBeanCollector().getConstructedName(objectName), configuration.getPresentationInformation());
					}
					try {
						answer.put(configuration.getMBeanCollector().getConstructedName(objectName),
								configuration.getMBeanCollector().getValues(objectName));
					} catch (Throwable t) {
						LOG.log(Level.SEVERE, t.getMessage(), t);
						continue;
					}					
				}
			}
		}
		return answer;
	}

	@Override
	public String collectMeasurementsAsJSON(String category)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {
		return gson.toJson(collectMeasurements(category));
	}

	@Override
	public Map<String, Object> collectMeasurements(String category, String name)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {

		for (Configuration configuration : configurations) {
			if (configuration.getPresentationInformation().getCategory().equals(category)) {
				for (ObjectName objectName : configuration.getMBeanFinder().getMatchingObjectNames()) {
					if (configuration.getMBeanCollector().getConstructedName(objectName).equals(name)) {
						return configuration.getMBeanCollector().getValues(objectName);
					}
				}
			}
		}
		return null;
	}

	@Override
	public String collectMeasurementsAsJSON(String category, String name)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {
		return gson.toJson(collectMeasurements(category, name));
	}

	@Override
	public Object collectMeasurement(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {

		return collectMeasurements(category, name).get(attribute);
	}

	public String collectMeasurementAsJSON(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException, MBeanException, AttributeNotFoundException, IntrospectionException {
		return gson.toJson(collectMeasurement(category, name, attribute));
	}

	private void deactivateThresholds() {
		LOG.log(Level.FINE, "Stopping threshold threads");

		for (ThresholdThread t : thresholdThreads.values()) {
			t.stopThread();
			t.interrupt();
		}		
		thresholdThreads.clear();
	}

	private void activateThresholds() {
		for (Configuration configuration : configurations) {
			for (Entry<String, Threshold> entry : configuration.getThresholds().entrySet()) {
				Threshold threshold = entry.getValue();

				if (!thresholdThreads.containsKey(threshold.getInterval())) {
					String threadName = "JMole Threshold Thread: " + threshold.getInterval() + " ms";
					LOG.log(Level.FINE, "Starting thread: " + threadName);
					ThresholdThread thread = new ThresholdThread(threadName, threshold.getInterval(), this);
					thread.start();
					thresholdThreads.put(threshold.getInterval(), thread);
				} else {
    					LOG.log(Level.FINE, "Thread already exists for " + threshold.getInterval());
				}
			}
		}
	}

	@Override
	public Map<String, Map<String, String>> warningMessages()
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return constructMessageData(true);
	}

	@Override
	public String warningMessagesAsJSON()
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return gson.toJson(constructMessageData(true));
	}

	@Override
	public Map<String, Map<String, String>> criticalMessages()
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return constructMessageData(false);
	}

	@Override
	public String criticalMessagesAsJSON()
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return gson.toJson(constructMessageData(false));
	}

	private Map<String, Map<String, String>> constructMessageData(boolean warning)
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		Map<String, Map<String, String>> answer = new HashMap<String, Map<String, String>>();

		for (ThresholdThread t : thresholdThreads.values()) {
			Set<Entry<ObjectName, String>> entrySet = null;
			if (warning) {
				entrySet = t.getWarningMessages().entrySet();
			} else {
				entrySet = t.getCriticalMessages().entrySet();
			}
			Map<String, String> thresholdMap = new HashMap<String, String>();
			String category = null;
			for (Entry<ObjectName, String> entry : entrySet) {
				ObjectName objectName = entry.getKey();
				String msg = entry.getValue();
				thresholdMap.put(t.getConfiguration(objectName).getMBeanCollector().getConstructedName(objectName), msg);
				category = t.getConfiguration(objectName).getPresentationInformation().getCategory();
			}
			if (!thresholdMap.isEmpty()) {
				answer.put(category, thresholdMap);
			}
                }

		return answer;
	}

	@Override
	public int getLevel() {
		return Integer.getInteger(CONFIG_LEVEL_PROPERTY, 3);
	}

	@Override
	public int getNumberOfThresholdThreads() {
		return thresholdThreads.size();
	}

	@Override
	public int getNumberOfConfigurations() {
		return configurations.size();
	}

	@Override
	public int getNumberOfThresholds() {
		int answer = 0;
		for (Configuration configuration : configurations) {
			answer += configuration.getThresholds().size();
		}
		return answer;
	}

	@Override
	public String[] getAllJMoleSystemProperties() {
		List<String> list = new ArrayList<String>();
		for (String key : System.getProperties().stringPropertyNames()) {
			if (key.startsWith("jmole.")) {
				list.add(key + "=" + System.getProperty(key));
			}
		}		
		String[] answer = new String[list.size()];
		answer = list.toArray(answer);
		return answer;
	}

}
