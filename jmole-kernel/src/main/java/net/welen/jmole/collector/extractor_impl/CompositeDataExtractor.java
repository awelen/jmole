package net.welen.jmole.collector.extractor_impl;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Properties;
import javax.management.ObjectName;

import javax.management.openmbean.CompositeData;

import net.welen.jmole.Utils;
import net.welen.jmole.collector.AbstractDataCollectorExtractor;


public class CompositeDataExtractor extends AbstractDataCollectorExtractor {

	private String attribute;
	private String name;
	
	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);

		attribute = properties.getProperty("attribute");
		if (attribute == null) {
			throw new IllegalArgumentException("Property attribute can't be null");
		}
		name = properties.getProperty("name");
		if (name == null) {
			throw new IllegalArgumentException("Property name can't be null");
		}
	}
	
	@Override
	public Object extractData(ObjectName objectName) throws Exception {
		Object compositeData = Utils.getMBeanServer().getAttribute(objectName, attribute);
		if (compositeData == null) {
			return null;
		} else if  (compositeData instanceof CompositeData) {
			return ((CompositeData) compositeData).get(name);
		} else {
			throw new IllegalArgumentException("Found attribute is not a compositeData: " + compositeData.getClass().getName());
		}
	}

}
