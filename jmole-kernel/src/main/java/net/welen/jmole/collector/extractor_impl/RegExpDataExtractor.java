package net.welen.jmole.collector.extractor_impl;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import net.welen.jmole.Utils;
import net.welen.jmole.collector.AbstractDataCollectorExtractor;

import java.util.Iterator;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.management.ObjectName;

public class RegExpDataExtractor extends AbstractDataCollectorExtractor {
	
	protected static String REGEXP_PREFIX = "regexp_"; 
	protected static String VALUE_PREFIX = "value_";
	
	private SortedMap<String, String> sortedMap = null;

	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
		sortedMap = new TreeMap(getProperties());			
	}

	@Override
	public Object extractData(ObjectName objectName) throws Exception {
		String data = Utils.getMBeanServer().getAttribute(objectName, getAttribute()).toString();
		Iterator<String> iterator = sortedMap.keySet().iterator();
	    while (iterator.hasNext()) {
	    	String key = iterator.next();	    	
	        if (key.startsWith(REGEXP_PREFIX)) {
	        	String regex = sortedMap.get(key);	        	
	        	String replacement = sortedMap.get(key.replace(REGEXP_PREFIX, VALUE_PREFIX));
	        	if (replacement == null) {
	        		throw new IllegalArgumentException("The regexp is missing a corresponding value: " + regex);
	        	}	        	
	        	data = data.replaceAll(regex, replacement);
	        }
	    }

	    return data;
	}

}
