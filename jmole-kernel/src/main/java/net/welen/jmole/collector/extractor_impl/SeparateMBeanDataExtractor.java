package net.welen.jmole.collector.extractor_impl;

import java.util.Hashtable;
import java.util.Properties;

import javax.management.ObjectName;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import net.welen.jmole.Utils;
import net.welen.jmole.collector.AbstractDataCollectorExtractor;

public class SeparateMBeanDataExtractor extends AbstractDataCollectorExtractor {
		
	private String attribute;
	private String objectNameString;

	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);

		attribute = getProperties().getProperty("attribute");
		if (attribute == null) {
			throw new IllegalArgumentException("Property attribute can't be null");
		}
		objectNameString = getProperties().getProperty("objectName");
		if (objectNameString == null) {
			throw new IllegalArgumentException("Property objectName can't be null");		
		}		
	}

	@Override
	public Object extractData(ObjectName parentObjectName) throws Exception {
		ObjectName objectName = new ObjectName(objectNameString);
		String parentMBeanParameterToAdd = getProperties().getProperty("parentMBeanParameterToAdd");
		if (parentMBeanParameterToAdd != null) {
			String domain = objectName.getDomain();
			Hashtable<String, String> list = objectName.getKeyPropertyList();
			list.put(parentMBeanParameterToAdd, 
					parentObjectName.getKeyProperty(parentMBeanParameterToAdd));
			objectName = new ObjectName(domain, list);
		}		
		
		return Utils.getMBeanServer().getAttribute(objectName, attribute);
	}

}
