package net.welen.jmole.presentation;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
import org.junit.Test;

public class PresentationInformationTest {

	@Test
	public void testSetCategory() {
		PresentationInformation pi = new PresentationInformation();
		String category = "Test";
		pi.setCategory(category);
		assertEquals(category, pi.getCategory());
	}

	@Test
	public void testSetUnit() {
		PresentationInformation pi = new PresentationInformation();
		String unit = "Test";
		pi.setUnit(unit);
		assertEquals(unit, pi.getUnit());
	}

	@Test
	public void testSetDescription() {
		PresentationInformation pi = new PresentationInformation();
		String description = "Test";
		pi.setDescription(description);
		assertEquals(description, pi.getDescription());
	}

	@Test
	public void testSetAttributeDescriptions() {
		PresentationInformation pi = new PresentationInformation();
		Map<String, String> attributeDescriptions = new HashMap<String, String>();
		attributeDescriptions.put("Test1", "Test2");
		pi.setAttributeDescriptions(attributeDescriptions);
		
		assertEquals("Test2", pi.getAttributeDescription("Test1"));
	}

	@Test
	public void testAttributeLabels() {
		PresentationInformation pi = new PresentationInformation();
		Map<String, String> attributeLabels = new HashMap<String, String>();
		attributeLabels.put("Test1", "Label");
		pi.setAttributeLabels(attributeLabels);
		
		assertEquals("Label", pi.getAttributeLabel("Test1"));
		assertEquals("Label", pi.translateAttributeLabel("Test1"));
		assertEquals("NonExisting", pi.translateAttributeLabel("NonExisting"));		
	}

}
