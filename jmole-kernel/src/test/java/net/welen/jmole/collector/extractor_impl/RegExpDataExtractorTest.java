package net.welen.jmole.collector.extractor_impl;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Before;
import org.junit.Test;

import net.welen.jmole.collector.extractor_impl.RegExpDataExtractor;

import static org.junit.Assert.*;

import java.util.Properties;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

public class RegExpDataExtractorTest {
	
	ObjectName objectName;
	
	@Before
	public void setup() throws MalformedObjectNameException {
		objectName = new ObjectName("java.lang:type=Runtime");
	}
	
	@Test
	public void testEmpty() throws Exception {
		RegExpDataExtractor extractor = new RegExpDataExtractor();						
		extractor.setAttribute("ObjectName");

		Properties props = new Properties();		
		extractor.setProperties(props);		
		assertEquals("java.lang:type=Runtime", extractor.extractData(objectName));		
	}

	@Test
	public void testSingle() throws Exception {
		RegExpDataExtractor extractor = new RegExpDataExtractor();						
		extractor.setAttribute("ObjectName");

		Properties props = new Properties();
		props.setProperty(RegExpDataExtractor.REGEXP_PREFIX + "1", "^.*");
		props.setProperty(RegExpDataExtractor.VALUE_PREFIX + "1", "New String");
		extractor.setProperties(props);
		assertEquals("New String", extractor.extractData(objectName));		
	}

	@Test
	public void testMultiple() throws Exception {
		RegExpDataExtractor extractor = new RegExpDataExtractor();						
		extractor.setAttribute("ObjectName");

		Properties props = new Properties();
		props.setProperty(RegExpDataExtractor.REGEXP_PREFIX + "1", "^.*");
		props.setProperty(RegExpDataExtractor.VALUE_PREFIX + "1", "New String");
		props.setProperty(RegExpDataExtractor.REGEXP_PREFIX + "2", "S");
		props.setProperty(RegExpDataExtractor.VALUE_PREFIX + "2", "X");
		extractor.setProperties(props);
		assertEquals("New Xtring", extractor.extractData(objectName));		
	}

}
