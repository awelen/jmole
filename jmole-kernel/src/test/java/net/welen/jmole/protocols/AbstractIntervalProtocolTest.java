package net.welen.jmole.protocols;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.*;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.Notification;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.welen.jmole.JMole;
import net.welen.jmole.Lifecycle;
import net.welen.jmole.Mock;
import net.welen.jmole.Utils;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.protocols.AbstractIntervalProtocol;

public class AbstractIntervalProtocolTest {
	
	private MBeanServer server = Utils.getMBeanServer();
	private ObjectName objectName1;
	private ObjectName objectName2; 

	@Before
	public void setup() throws MalformedObjectNameException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {		

		objectName1 = new ObjectName("jmole.test:test=1");
		objectName2 = new ObjectName("jmole.test:test=2");
		Mock mock1 = new Mock();
		mock1.setAttribute(11);
		Mock mock2 = new Mock();
		mock2.setAttribute(21);
		server.registerMBean(mock1, objectName1);
		server.registerMBean(mock2, objectName2);
	}

	@After
	public void cleanup() throws MBeanRegistrationException, InstanceNotFoundException {
		server.unregisterMBean(objectName1);
		server.unregisterMBean(objectName2);
	}
	
	@Test
	public void testThreadHandling() throws Exception {
		System.setProperty(JMole.CONFIG_FILENAME_PROPERTY_PREFIX + "TEST", "src/test/resources/JMole_Test.xml");
		System.setProperty("JMOLE_TEST_LEVEL", "1");
		System.setProperty("jmole.protocol.testprotocol.enabled", "true");
		System.setProperty("jmole.protocol.testprotocol.classname", TestProtocol.class.getName());
		try {
			Lifecycle.setup();
			assertEquals(0, TestProtocol.handleWarningsCalls);
                        assertEquals(0, TestProtocol.handleCriticalsCalls);
                        assertEquals(0, TestProtocol.handleMeasurementCalls);
			Thread.sleep(3000);
			assertTrue(TestProtocol.handleWarningsCalls > 0);
			assertTrue(TestProtocol.handleCriticalsCalls > 0);
			assertTrue(TestProtocol.handleMeasurementCalls > 0);
		} finally {
			System.clearProperty(JMole.CONFIG_FILENAME_PROPERTY_PREFIX + "TEST");
                	System.clearProperty(JMole.CONFIG_FILENAME_XSLT_PROPERTY_PREFIX + "TEST");
               		System.clearProperty("JMOLE_TEST_LEVEL");
               		System.clearProperty("jmole.protocol.testprotocol.enabled");
               		System.clearProperty("jmole.protocol.testprotocol.classname");
			Lifecycle.cleanup();
		}			
	}

}
