package net.welen.jmole.protocols;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import net.welen.jmole.JMole;
import net.welen.jmole.threshold.JMoleNotification;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.protocols.AbstractIntervalProtocol;

import javax.management.Notification;

public class TestProtocol extends AbstractIntervalProtocol implements TestProtocolMBean {

	public static int handleMeasurementCalls = 0;
	public static int handleWarningsCalls = 0;
	public static int handleCriticalsCalls = 0;

	@Override
	public void startProtocol(JMole jmole) throws Exception {
		super.startProtocol(jmole);
                setInterval(1000);
		setName("TestProtocol");
	}
	
	@Override
	protected void handleMeasurement(String category, String name, String attribute, Object value,
			PresentationInformation presentationInformation) throws Exception {
		handleMeasurementCalls++;
	}

	@Override
        public void handleWarning(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		handleWarningsCalls++;
        }

        @Override
        public void handleCritical(String name, String attribute, PresentationInformation presentationInformation, String message) throws Exception {
		handleCriticalsCalls++;
        }

}
