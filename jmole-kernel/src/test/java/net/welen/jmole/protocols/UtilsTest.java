package net.welen.jmole.protocols;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2023 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Map;
import java.util.HashMap;
import net.welen.jmole.presentation.PresentationInformation;

public class UtilsTest {

	@Test
	public void formatLogStringTest() {
		PresentationInformation presentationInformation = new PresentationInformation();
		presentationInformation.setUnit("TestUnit");
		Map<String, String> attributeDescriptions = new HashMap();
		attributeDescriptions.put("TestAttribute", "TestAttributeDescription");
		presentationInformation.setAttributeDescriptions(attributeDescriptions);

		// Well behaved
		String formatted = Utils.formatLogString("%c %n %t %k %a %A %v %U", '/', "TestCategory", "TestName", "TestAttribute", "TestValue", presentationInformation);
		assertEquals("TestCategory TestName TestAttribute TestCategory/TestName/TestAttribute TestAttribute TestAttributeDescription TestValue TestUnit", formatted);

		// Issue #172
		formatted = Utils.formatLogString("%c %n %t %k %a %A %v %U", '/', "TestCategory", "Test${test}Name", "TestAttribute", "TestValue", presentationInformation);
		assertEquals("TestCategory Test${test}Name TestAttribute TestCategory/Test${test}Name/TestAttribute TestAttribute TestAttributeDescription TestValue TestUnit", formatted);

	}
}
